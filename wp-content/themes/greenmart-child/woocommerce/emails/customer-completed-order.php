<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<p><?php printf( esc_html__( 'Ciao %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
<?php /* translators: %s: Site title */ ?>
<p>Abbiamo terminato l’elaborazione dell’ordine che verrà spedito lunedì. Riceverai una mail con il tracking dal corriere. Per il rispetto dell'ambiente abbiamo pensato, anziché stamparli, di inoltrarti dei consigli utili per la conservazione dei nostri frutti in PDF <a href="https://drive.google.com/file/d/117w8huwv89PHLuxBbZTGjZrIaA8e50e3/view?usp=sharing" style="color: #86bc42; font-weight: bold;">CLICCA QUI PER AVOCADO HASS</a> <a href="https://drive.google.com/file/d/1nkWmpJhhXE8im-zrCS0evCr1yRNAXXJ8/view?usp=sharing" style="color: #86bc42; font-weight: bold;">CLICCA QUI PER LA PAPAYA</a> <a href="https://drive.google.com/file/d/1NUQ8qCe77i5I8-gygzql9BB-j1JR3w14/view?usp=sharing" style="color: #86bc42; font-weight: bold;">CLICCA QUI PER IL MANGO</a></p>
<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

?>
<p>
Grazie per averci scelto.
</p>
<?php

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );