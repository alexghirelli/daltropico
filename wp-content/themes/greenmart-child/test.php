<?php /* Template Name: test Template */ 

    $order_data = $order->get_data();
    $order_meta = get_post_meta('92594');

    foreach ($order->get_items() as $item ) {
        $item_data    = $item->get_data();

        $order_data['line_items'] = [];
        $order_data['line_items'][] = [
            'name' => $item_data['name'],
            'quantity' => $item_data['quantity'],
            'subtotal' => $item_data['subtotal'],
            'total' => $item_data['total']
        ];
    }

    $order_data['meta_data'] = $order_meta;
    
    print_r(json_encode($order_data));
?>