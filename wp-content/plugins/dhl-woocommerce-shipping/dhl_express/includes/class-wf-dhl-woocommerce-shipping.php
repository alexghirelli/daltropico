<?php

if (!defined('ABSPATH')) {
    exit;
}

class wf_dhl_woocommerce_shipping_method extends WC_Shipping_Method {

    private $found_rates;
    private $services;

    public function __construct() {
        if( !function_exists('is_plugin_active') ) {        
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        }
        $this->id = WF_DHL_ID;
        $this->method_title = __('DHL Express', 'wf-shipping-dhl');
        $this->method_description = '';
        $this->services = include( 'data-wf-service-codes.php' );
        $this->init();  
        $this->aelia_activated = is_plugin_active('woocommerce-aelia-currencyswitcher/woocommerce-aelia-currencyswitcher.php')? true: false;
        
        // $this->dokan_litr_activated = is_plugin_active('dokan-lite/dokan.php')? true: false;
        // $this->dokan_pro_activated = is_plugin_active('dokan-pro/dokan-pro.php')? true: false;
        // $this->xa_multivendor_activated = is_plugin_active('multi-vendor-add-on-for-thirdparty-shipping/multi-vendor-add-on-for-thirdparty-shipping.php')? true: false;
    }
    private function init() {
       
        include_once('data-wf-default-values.php');
        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        // Define user set variables
        $this->enabled                  = isset( $this->settings['enabled'] ) ? $this->settings['enabled'] : 'no';
        $this->title                    = $this->get_option('title', $this->method_title);
        $this->availability             = isset( $this->settings['availability'] ) ? $this->settings['availability'] : 'all';
        $this->countries                = isset( $this->settings['countries'] ) ? $this->settings['countries'] : array();
        $this->origin                   = apply_filters('woocommerce_dhl_origin_postal_code', str_replace(' ', '', strtoupper($this->get_option('origin'))));
        $selected_country               = isset($this->settings['base_country']) ? $this->settings['base_country'] : WC()->countries->get_base_country();
        $this->origin_country           = apply_filters('woocommerce_dhl_origin_country_code', $selected_country);
        $this->origin_country_1         = $this->origin_country;
        $this->account_number           = $this->get_option('account_number');
        $this->site_id                  = $this->get_option('site_id');
        $this->site_password            = $this->get_option('site_password');
        $this->show_dhl_extra_charges   = $this->get_option('show_dhl_extra_charges');
        $this->show_dhl_insurance_charges   = $this->get_option('show_dhl_insurance_charges');
        $this->freight_shipper_city     = $this->get_option('freight_shipper_city');
        $this->freight_shipper_city_1   = $this->freight_shipper_city;
        $del_bool                       =  $this->get_option( 'delivery_time' );
        $this->delivery_time            = ($del_bool == 'yes') ? true : false;
        $this->latin_encoding = isset($this->settings['latin_encoding']) && $this->settings['latin_encoding'] == 'yes' ? true : false;
        $utf8_support = $this->latin_encoding ? '?isUTF8Support=true' : '';

        $_stagingUrl                    = 'https://xmlpitest-ea.dhl.com/XMLShippingServlet'.$utf8_support;
        $_productionUrl                 = 'https://xmlpi-ea.dhl.com/XMLShippingServlet'.$utf8_support;

        $this->production               = (!empty($this->settings['production']) && $this->settings['production']  === 'yes') ? true : false;
        $this->service_url              = ($this->production == true) ? $_productionUrl : $_stagingUrl;

        $debug_bool                     = $this->get_option('debug');
        $this->debug                    = ($debug_bool  == 'yes') ? true : false;
        $insurance_bool                 = $this->get_option('insure_contents');
        $this->insure_contents          = ($insurance_bool == 'yes') ? true : false;
        
        $this->request_type             = $this->get_option('request_type', 'LIST');
        $this->packing_method           = $this->get_option('packing_method', 'per_item');
        $this->boxes                    = $this->get_option('boxes');
        $this->custom_services          = $this->get_option('services', array());
        $this->offer_rates              = $this->get_option('offer_rates', 'all');

        $this->dutypayment_type         = $this->get_option('dutypayment_type', '');
        $this->dutyaccount_number       = $this->get_option('dutyaccount_number', '');

        $this->dimension_unit           = $this->get_option('dimension_weight_unit') == 'LBS_IN' ? 'IN' : 'CM';
        $this->weight_unit              = $this->get_option('dimension_weight_unit') == 'LBS_IN' ? 'LBS' : 'KG';

        $this->quoteapi_dimension_unit  = $this->dimension_unit;
        $this->quoteapi_weight_unit     = $this->weight_unit == 'LBS' ? 'LB' : 'KG';
        
        $this->conversion_rate          = !empty($this->settings['conversion_rate']) ? $this->settings['conversion_rate'] : '';

        $this->conversion_rate          = apply_filters('wf_dhl_conversion_rate', $this->conversion_rate, $this->settings['dhl_currency_type']);
        
        //Time zone adjustment, which was configured in minutes to avoid time diff with server. Convert that in seconds to apply in date() functions.
        $this->timezone_offset          = !empty($this->settings['timezone_offset']) ? intval($this->settings['timezone_offset']) * 60 : 0;
        
        $this->insure_currency          = isset( $this->settings['insure_currency'] ) ?  $this->settings['insure_currency'] : '';
        $this->insure_converstion_rate  = !empty($this->settings['insure_converstion_rate']) ? $this->settings['insure_converstion_rate'] : '';
        
        if(class_exists('wf_vendor_addon_setup'))
        {
            if(isset($this->settings['vendor_check']) && $this->settings['vendor_check'] === 'yes')
            {
                $this->ship_from_address = 'vendor_address'; 
            }
            else
            {
                $this->ship_from_address = 'origin_address';
            }
        }else
        {
            $this->ship_from_address = 'origin_address';
        }
        
        $this->weight_packing_process   = !empty($this->settings['weight_packing_process']) ? $this->settings['weight_packing_process'] : 'pack_descending';
        $this->box_max_weight           = !empty($this->settings['box_max_weight']) ? $this->settings['box_max_weight'] : '';
        $this->dhl_insurance_at_checkout = 'no';
        $this->http_req_referer = '';
        $this->general_settings = get_option('woocommerce_wf_dhl_shipping_settings');

        add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
        $this->woo_countries = new WC_Countries();
        // $this->cart = new WC_Cart();
    }

    /**
     * is_available function.
     *
     * @param array $package
     * @return bool
     */
    public function is_available( $package ) {
        if ( "no" === $this->enabled || empty($this->enabled ) ) {
            return false;
        }

        if ( 'specific' === $this->availability ) {
            if ( is_array( $this->countries ) && ! in_array( $package['destination']['country'], $this->countries ) ) {
                return false;
            }
        } elseif ( 'excluding' === $this->availability ) {
            if ( is_array( $this->countries ) && ( in_array( $package['destination']['country'], $this->countries ) || ! $package['destination']['country'] ) ) {
                return false;
            }
        }
        return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', true, $package );
    }

    public function debug($message, $type = 'notice') {
        if ($this->debug) {
            wc_add_notice($message, $type);
        }
    }

    public function admin_options() {
       
        // Show settings
        parent::admin_options();
    }

    public function init_form_fields() {
        if(isset($_GET['page']) && $_GET['page'] === 'wc-settings')
        {
            $this->form_fields = include( 'data-wf-settings.php' );
        }
    }

    public function generate_activate_box_html() {
        // ob_start();
        // $plugin_name = 'dhl';
        // include( WF_DHL_PAKET_EXPRESS_ROOT_PATH . 'wf_api_manager/html/html-wf-activation-window.php' );
        // return ob_get_clean();
    }
    public function generate_wf_dhl_tab_box_html() {

        $tab = (!empty($_GET['subtab'])) ? esc_attr($_GET['subtab']) : 'general';

                echo '
                <div class="wrap">
                    <style>
                        .woocommerce-help-tip{color:darkgray !important;}
                        .woocommerce-save-button{display:none !important;}
                        <style>
                        .woocommerce-help-tip {
                            position: relative;
                            display: inline-block;
                            border-bottom: 1px dotted black;
                        }

                        .woocommerce-help-tip .tooltiptext {
                            visibility: hidden;
                            width: 120px;
                            background-color: black;
                            color: #fff;
                            text-align: center;
                            border-radius: 6px;
                            padding: 5px 0;

                            /* Position the tooltip */
                            position: absolute;
                            z-index: 1;
                        }

                        .woocommerce-help-tip:hover .tooltiptext {
                            visibility: visible;
                        }
                        </style>
                    </style>
                    <hr class="wp-header-end">';
                $this->wf_dhl_shipping_page_tabs($tab);
                switch ($tab) {
                    case "general":
                        echo '<div class="table-box table-box-main" id="general_section" style="margin-top: 0px;border: 1px solid #ccc;border-top: unset !important;padding: 5px;">';
                        require_once('settings/dhl_general_settings.php');
                        echo '</div>';
                        break;
                    case "rates":
                        echo '<div class="table-box table-box-main" id="rates_section" style="margin-top: 0px;border: 1px solid #ccc;border-top: unset !important;padding: 5px;">';
                        require_once('settings/dhl_rates_settings.php');
                        echo '</div>';
                        break;
                    case "labels":
                        echo '<div class="table-box table-box-main" id="labels_section" style="margin-top: 0px;border: 1px solid #ccc;border-top: unset !important;padding: 5px;">';
                        require_once('settings/dhl_labels_settings.php');
                        echo '</div>';
                        break;
                    case "packing":
                        echo '<div class="table-box table-box-main" id="packing_section" style="margin-top: 0px;border: 1px solid #ccc;border-top: unset !important;padding: 5px;">';
                        require_once('settings/dhl_packing_settings.php');
                        echo '</div>';
                        break;
                    case "licence":
                        echo '<div class="table-box table-box-main" id="licence_section" style="margin-top: 0px;border: 1px solid #ccc;border-top: unset !important;padding: 5px;">';
                        $plugin_name = 'dhl';
                        include( WF_DHL_PAKET_EXPRESS_ROOT_PATH . 'wf_api_manager/html/html-wf-activation-window.php' );
                        echo '</div>';
                        break;
                }
                echo '
                </div>';


       }
    
       public function wf_dhl_shipping_page_tabs($current = 'general')
       {
            $activation_check = get_option('dhl_activation_status');
            if(!empty($activation_check) && $activation_check === 'active')
            {
                $acivated_tab_html =  "<small style='color:green;font-size:xx-small;'>(Activated)</small>";

            }
            else
            {
                $acivated_tab_html =  "<small style='color:red;font-size:xx-small;'>(Activate)</small>";
            }
            $tabs = array(
                        'general' => __("General", 'wf-shipping-dhl'),
                        'rates' => __("Rates & Services", 'wf-shipping-dhl'),
                        'labels' => __("Label & Tracking", 'wf-shipping-dhl'),
                        'packing' => __("Packaging", 'wf-shipping-dhl'),
                        'licence' => __("License ".$acivated_tab_html, 'wf-shipping-dhl')
                    );
            $html = '<h2 class="nav-tab-wrapper">';
            foreach ($tabs as $tab => $name) {
                $class = ($tab == $current) ? 'nav-tab-active' : '';
                $style = ($tab == $current) ? 'border-bottom: 1px solid transparent !important;' : '';
                $html .= '<a style="text-decoration:none !important;' . $style . '" class="nav-tab ' . $class . '" href="?page='.wf_get_settings_url().'&tab=shipping&section=wf_dhl_shipping&subtab=' . $tab . '">' . $name . '</a>';
            }
            $html .= '</h2>';
            echo $html;
        }
    

    public function generate_services_html() {
        ob_start();
        include( 'html-wf-services.php' );
        return ob_get_clean();
    }

    public function generate_box_packing_html() {
        ob_start();
        include( 'html-wf-box-packing.php' );
        return ob_get_clean();
    }

    public function validate_box_packing_field($key) {
        $boxes_id           = isset($_POST['boxes_id']) ? $_POST['boxes_id'] : array();
        $boxes_name         = isset($_POST['boxes_name']) ? $_POST['boxes_name'] : array();
        $boxes_length       = isset($_POST['boxes_length']) ? $_POST['boxes_length'] : array();
        $boxes_width        = isset($_POST['boxes_width']) ? $_POST['boxes_width'] : array();
        $boxes_height       = isset($_POST['boxes_height']) ? $_POST['boxes_height'] : array();
        $boxes_inner_length = isset($_POST['boxes_inner_length']) ? $_POST['boxes_inner_length'] : array();
        $boxes_inner_width  = isset($_POST['boxes_inner_width']) ? $_POST['boxes_inner_width'] : array();
        $boxes_inner_height = isset($_POST['boxes_inner_height']) ? $_POST['boxes_inner_height'] : array();
        
        $boxes_box_weight   = isset($_POST['boxes_box_weight']) ? $_POST['boxes_box_weight'] : array();
        $boxes_max_weight   = isset($_POST['boxes_max_weight']) ? $_POST['boxes_max_weight'] : array();
        $boxes_enabled      = isset($_POST['boxes_enabled']) ? $_POST['boxes_enabled'] : array();
        $boxes_pack_type    = isset($_POST['boxes_pack_type']) ? $_POST['boxes_pack_type'] : array();

        $boxes = array();

        if (!empty($boxes_length) && sizeof($boxes_length) > 0) {
            for ($i = 0; $i <= max(array_keys($boxes_length)); $i ++) {

                if (!isset($boxes_length[$i]))
                    continue;

                if ($boxes_length[$i] && $boxes_width[$i] && $boxes_height[$i]) {

                    $boxes[] = array(
                        'id' => $boxes_id[$i],
                        'name' => $boxes_name[$i],
                        'length' => floatval($boxes_length[$i]),
                        'width' => floatval($boxes_width[$i]),
                        'height' => floatval($boxes_height[$i]),
                        'inner_length' => floatval($boxes_inner_length[$i]),
                        'inner_width' => floatval($boxes_inner_width[$i]),
                        'inner_height' => floatval($boxes_inner_height[$i]),
                        'box_weight' => floatval($boxes_box_weight[$i]),
                        'max_weight' => floatval($boxes_max_weight[$i]),
                        'enabled' => isset($boxes_enabled[$i]) ? true : false,
                        'pack_type' => $boxes_pack_type[$i]
                    );
                }
            }
        }
        return $boxes;
    }

    public function validate_services_field($key) {
        $services = array();
        $posted_services = $_POST['dhl_service'];

        foreach ($posted_services as $code => $settings) {
            $services[$code] = array(
                'name' => wc_clean($settings['name']),
                'order' => wc_clean($settings['order']),
                'enabled' => isset($settings['enabled']) ? true : false,
                'adjustment' => wc_clean($settings['adjustment']),
                'adjustment_percent' => str_replace('%', '', wc_clean($settings['adjustment_percent']))
            );
        }

        return $services;
    }

    public function get_dhl_packages($package) {
        switch ($this->packing_method) {
            case 'box_packing' :
                return $this->box_shipping($package);
                break;
            case 'weight_based' :
                return $this->weight_based_shipping($package);
                break;
            case 'per_item' :
            default :
                return $this->per_item_shipping($package);
                break;
        }
    }

    /**
     * weight_based_shipping function.
     *
     * @access private
     * @param mixed $package
     * @return void
    **/
    private function weight_based_shipping($package) {
        global $woocommerce;
        if ( ! class_exists( 'WeightPack' ) ) {
            include_once('weight_pack/class-wf-weight-packing.php');
        }
        $weight_pack=new WeightPack($this->weight_packing_process);
        $weight_pack->set_max_weight($this->box_max_weight);
        
        $package_total_weight = 0;
        $insured_value = 0;
        
        
        $ctr = 0;
        foreach ($package['contents'] as $item_id => $values) {
            $ctr++;
            
            $skip_product = apply_filters('wf_shipping_skip_product_from_dhl_label',false, $values, $package['contents']);
            if($skip_product){
                continue;
            }
            
            if (!($values['quantity'] > 0 && $values['data']->needs_shipping())) {
                $this->debug(sprintf(__('Product #%d is virtual. Skipping.', 'wf-shipping-dhl'), $ctr));
                continue;
            }

            if (!$values['data']->get_weight()) {
                $this->debug(sprintf(__('Product #%d is missing weight.', 'wf-shipping-dhl'), $ctr), 'error');
                return;
            }
            $weight_pack->add_item(wc_get_weight( $values['data']->get_weight(), $this->weight_unit ), $values['data'], $values['quantity']);
        }
        
        $pack   =   $weight_pack->pack_items();  
        $errors =   $pack->get_errors();
        $to_ship  = array();
        
        if( !empty($errors) ){
            //do nothing
            return;
        } else {
            $boxes    =   $pack->get_packed_boxes();
            $unpacked_items =   $pack->get_unpacked_items();
            
            $parcels      =   array_merge( $boxes, $unpacked_items ); // merge items if unpacked are allowed
            $parcel_count  =   sizeof($parcels);
            // get all items to pass if item info in box is not distinguished
            $packable_items =   $weight_pack->get_packable_items();
            $all_items    =   array();
            if(is_array($packable_items)){
                foreach($packable_items as $packable_item){
                    $all_items[]    =   $packable_item['data'];
                }
            }
            
            $order_total = '';
            if(isset($this->order)){
                $order_total    =   $this->order->get_total();;
            }
            
            $group_id = 1;
            foreach($parcels as $parcel){
                $insured_value        =   0;
                $packed_products = array();
                
                if(!empty($parcel['items'])){
                    foreach($parcel['items'] as $item){                        
                        $insured_value  =   $insured_value + $item->get_price();
                    }
                }
                
                $packed_products    =   isset($parcel['items']) ? $parcel['items'] : $all_items;
                // Creating package request
                $package_total_weight   = $parcel['weight'];
                $insurance_array = array(
                    'Amount' => round($insured_value),
                    'Currency' => get_woocommerce_currency()
                );
                
                $group = array(
                    'GroupNumber' => $group_id,
                    'GroupPackageCount' => 1,
                    'Weight' => array(
                        'Value' => round($package_total_weight, 3),
                        'Units' => $this->weight_unit
                    ),
                    'packed_products' => $packed_products,
                );
                $group['InsuredValue'] = $insurance_array;
                $group['packtype'] = isset($this->settings['shp_pack_type'])?$this->settings['shp_pack_type'] : 'OD';
                
                $to_ship[] = $group;
                $group_id++;
            }
        }
        return $to_ship;
    }


    private function per_item_shipping($package) {
        $to_ship = array();
        $group_id = 1;

        // Get weight of order
        foreach ($package['contents'] as $item_id => $values) {

            if (!$values['data']->needs_shipping()) {
                $this->debug(sprintf(__('Product # is virtual. Skipping.', 'wf-shipping-dhl'), $item_id), 'error');
                continue;
            }

            $skip_product = apply_filters('wf_shipping_skip_product_from_dhl_rate',false, $values, $package['contents']);
            if($skip_product){
                continue;
            }

            if (!$values['data']->get_weight()) {
                $this->debug(sprintf(__('Product # is missing weight. Aborting.', 'wf-shipping-dhl'), $item_id), 'error');
                return;
            }

            $group = array();
            $insurance_array = array(
                'Amount' => round($values['data']->get_price()),
                'Currency' => get_woocommerce_currency()
            );

            if(wc_get_weight($values['data']->get_weight(), $this->weight_unit)<0.001){
                $xa_per_item_weight = 0.001;
            }else{
                $xa_per_item_weight = round(wc_get_weight($values['data']->get_weight(), $this->weight_unit), 3);
            }
            $group = array(
                'GroupNumber' => $group_id,
                'GroupPackageCount' => 1,
                'Weight' => array(
                    'Value' => $xa_per_item_weight,
                    'Units' => $this->weight_unit
                ),
                'packed_products' => array($values['data'])
            );
            
            if ( wf_get_product_length( $values['data'] ) && wf_get_product_height( $values['data'] ) && wf_get_product_width( $values['data'] )) {

                $dimensions = array( wf_get_product_length( $values['data'] ), wf_get_product_width( $values['data'] ), wf_get_product_height( $values['data'] ));

                sort($dimensions);

                $group['Dimensions'] = array(
                    'Length' => max(1, round(wc_get_dimension($dimensions[2], $this->dimension_unit), 0)),
                    'Width' => max(1, round(wc_get_dimension($dimensions[1], $this->dimension_unit), 0)),
                    'Height' => max(1, round(wc_get_dimension($dimensions[0], $this->dimension_unit), 0)),
                    'Units' => $this->dimension_unit
                );
            }
            $group['packtype'] = isset($this->settings['shp_pack_type'])?$this->settings['shp_pack_type'] : 'BOX';
            $group['InsuredValue'] = $insurance_array;

            for ($i = 0; $i < $values['quantity']; $i++)
                $to_ship[] = $group;

            $group_id++;
        }

        return $to_ship;
    }

    private function box_shipping($package) {
        if (!class_exists('WF_Boxpack')) {
            include_once('class-wf-packing.php');
        }

        $boxpack = new WF_Boxpack();

        // Define boxes
        foreach ($this->boxes as $key => $box) {
            if (!$box['enabled']) {
                continue;
            }
            $box['pack_type'] = !empty($box['pack_type']) ? $box['pack_type'] : 'BOX' ;
            
            $newbox = $boxpack->add_box($box['length'], $box['width'], $box['height'], $box['box_weight'], $box['pack_type']);

            if (isset($box['id'])) {
                $newbox->set_id(current(explode(':', $box['id'])));
            }

            if ($box['max_weight']) {
                $newbox->set_max_weight($box['max_weight']);
            }
            if ($box['pack_type']) {
                $newbox->set_packtype($box['pack_type']);
            }
        }

        // Add items
        foreach ($package['contents'] as $item_id => $values) {
            $values_data = $values['data'];
            $product_label = '';

            if(WC()->version < '2.7.0'){
                $values_data_post = $values_data->post;
                $values_data_post_title = $values_data_post->post_title;
                $product_label = $values_data_post_title;
            }else{
                $product_data = $values_data->get_data();
                $product_label = $product_data['name'];
            }

            if (!$values['data']->needs_shipping()) {
                $this->debug(sprintf(__('Product '.$product_label.' is virtual. Skipping.', 'wf-shipping-dhl'), $item_id), 'error');
                continue;
            }

            $skip_product = apply_filters('wf_shipping_skip_product_from_dhl_rate',false, $values, $package['contents']);
            if($skip_product){
                continue;
            }

            if ( wf_get_product_length( $values['data'] ) && wf_get_product_height( $values['data'] ) && wf_get_product_width( $values['data'] ) && wf_get_product_weight( $values['data'] )) {

                $dimensions = array( wf_get_product_length( $values['data'] ), wf_get_product_height( $values['data'] ), wf_get_product_width( $values['data'] ));

                for ($i = 0; $i < $values['quantity']; $i ++) {
                    $boxpack->add_item(
                            wc_get_dimension($dimensions[2], $this->dimension_unit), wc_get_dimension($dimensions[1], $this->dimension_unit), wc_get_dimension($dimensions[0], $this->dimension_unit), wc_get_weight($values['data']->get_weight(), $this->weight_unit), $values['data']->get_price(), array(
                        'data' => $values['data']
                            )
                    );
                }
            } else {
                if(!wf_get_product_weight( $values['data'] )){
                    $product_weight = wf_get_product_weight( $values['data']);

                    for ($i = 0; $i < $values['quantity']; $i ++) {
                        if(WC()->version < '2.7.0'){
                            $boxpack->add_item(wc_get_dimension($values_data['variation_has_length'], $this->dimension_unit), wc_get_dimension($values_data['variation_has_width'], $this->dimension_unit), wc_get_dimension($values_data['variation_has_height'], $this->dimension_unit), wc_get_weight($product_weight, $this->weight_unit), $values['data']->get_price(), array(
                            'data' => $values['data']));
                        }else{
                            $boxpack->add_item(wc_get_dimension($product_data['length'], $this->dimension_unit), wc_get_dimension($product_data['width'], $this->dimension_unit), wc_get_dimension($product_data['height'], $this->dimension_unit), wc_get_weight($product_weight, $this->weight_unit), $values['data']->get_price(), array(
                            'data' => $values['data']));
                        }
                    }

                 }
            }
        }

        // Pack it
        $boxpack->pack();
        $packages = $boxpack->get_packages();
        $to_ship = array();
        $group_id = 1;

        foreach ($packages as $package) {
            if ($package->unpacked === true) {
                $this->debug('Unpacked Item');
            } else {
                $this->debug('Packed ' . $package->id);
            }

            $dimensions = array($package->length, $package->width, $package->height);

            sort($dimensions);
            $insurance_array = array(
                'Amount' => round($package->value),
                'Currency' => get_woocommerce_currency()
            );
            
            $group = array(
                'GroupNumber' => $group_id,
                'GroupPackageCount' => 1,
                'Weight' => array(
                    'Value' => round($package->weight, 3),
                    'Units' => $this->weight_unit
                ),
                'Dimensions' => array(
                    'Length' => max(1, round($dimensions[2], 0)),
                    'Width' => max(1, round($dimensions[1], 0)),
                    'Height' => max(1, round($dimensions[0], 0)),
                    'Units' => $this->dimension_unit
                ),
                'InsuredValue' => $insurance_array,
                'packed_products' => array(),
                'package_id' => $package->id,
                'packtype' => isset($package->packtype)?$package->packtype:'BOX'
            );

            if (!empty($package->packed) && is_array($package->packed)) {
                foreach ($package->packed as $packed) {
                    $group['packed_products'][] = $packed->get_meta('data');
                }
            }

            $to_ship[] = $group;

            $group_id++;
        }

        return $to_ship;
    }

    /*
    * function returns the collection of countries which different WooCommerce country codes and DHL accepted country codes
    * @access public
    */
    public function dhl_country_codes_with_conflicts(){
        $countries = array( 
            'Bonaire' => array(
                'Woocommerce_country_code' => 'BQ',
                'dhl_country_code' => 'XB'
            ),
            'Curacao' => array(
                'Woocommerce_country_code' => 'CW',
                'dhl_country_code' => 'XC'
            ),
        );

        return $countries;
    }

    /*
    * function returns DHL accepted country codes for a given WooCommerce country codes
    * @access public
    */
    public function get_country_codes_mapped_for_dhl($country_code){
        $conflict_countries_codes = $this->dhl_country_codes_with_conflicts();

        foreach($conflict_countries_codes as $conflict_countries_codes_key => $conflict_countries_codes_values){
            if($conflict_countries_codes_values['Woocommerce_country_code'] === $country_code){
                return $conflict_countries_codes_values['dhl_country_code'];
            }
        }
        return $country_code;
    }

    private function get_dhl_requests($dhl_packages, $package) {
        global $woocommerce;

        // Time is modified to avoid date diff with server.
        $mailing_date = current_time('Y-m-d');
        $mailing_datetime = date('Y-m-d', current_time('timestamp')) . 'T' . date('H:i:s', current_time('timestamp'));
        $destination_postcode = str_replace(' ', '', strtoupper($package['destination']['postcode']));
        $pieces = $this->wf_get_package_piece($dhl_packages);

        $total_value = $woocommerce->cart->cart_contents_total;
        $currency = get_woocommerce_currency();
        $total_insurance_value = 0;
        $default_currency = $this->wf_get_currency_based_on_country_code(WC()->countries->get_base_country());
        $is_dutiable = '';
        $origin_postcode_city = '';
        $paymentCountryCode = '';
        
        if ($this->settings['insure_contents'] == 'yes' && !empty($this->insure_converstion_rate)){
            $total_insurance_value = round(apply_filters('wc_aelia_cs_convert', $total_value, get_woocommerce_currency(), $default_currency)) * $this->insure_converstion_rate;
        }else{
            $total_insurance_value = $total_value;
        }
        
        $insurance_details = "";
        $additional_insurance_details ="";
        $package['dhl_insurance'] = (isset($package['dhl_insurance']) && !empty($package['dhl_insurance']))? $package['dhl_insurance'] : false;
        
        $insurance_enabled = get_option('dhl_insurance');
        $insurance_enabled_at_checkout = 'no';

        $this->insure_currency = (!empty($this->insure_currency)) ? $this->insure_currency : get_woocommerce_currency();

        $insurance_enabled_at_checkout = get_option('dhl_insurance_at_checkout');

        if(empty($insurance_enabled_at_checkout)){
            $insurance_enabled_at_checkout = 'no';
        }

        if(strpos($this->http_req_referer, 'checkout') > 0){
            if($insurance_enabled_at_checkout == 'yes'){
                $package['dhl_insurance'] = true;
            }else{
                if($insurance_enabled == 'yes'){
                    $package['dhl_insurance'] = true;    
                }else{
                    $package['dhl_insurance'] = false;
                }
            }
        }
        
        if(is_shop()){
            if($insurance_enabled == 'yes'){
                if(get_option('dhl_insurance') == 'yes'){
                    $insurance_details = $this->insure_contents ? "<InsuredValue>{$total_insurance_value}</InsuredValue><InsuredCurrency>{$this->insure_currency}</InsuredCurrency>" : "";
                    $additional_insurance_details = ($this->insure_contents && ($this->conversion_rate || $this->insure_converstion_rate)) ? "<QtdShp><QtdShpExChrg><SpecialServiceType>II</SpecialServiceType><LocalSpecialServiceType>XCH</LocalSpecialServiceType></QtdShpExChrg></QtdShp>" : "";
                }
            }
            update_option("dhl_total_insurance_value", $total_insurance_value);
        }else if(is_cart())
        {   
            $this->insure_currency = (!empty($this->insure_currency)) ? $this->insure_currency : get_woocommerce_currency();
            if($insurance_enabled == 'yes'){
                $insurance_details = $this->insure_contents ? "<InsuredValue>{$total_insurance_value}</InsuredValue><InsuredCurrency>{$this->insure_currency}</InsuredCurrency>" : "";
                $additional_insurance_details = ($this->insure_contents && ($this->conversion_rate || $this->insure_converstion_rate)) ? "<QtdShp><QtdShpExChrg><SpecialServiceType>II</SpecialServiceType><LocalSpecialServiceType>XCH</LocalSpecialServiceType></QtdShpExChrg></QtdShp>" : "";
            }
            //insurance value
            update_option("dhl_total_insurance_value", $total_insurance_value);
        }else{
            $this->insure_currency = (!empty($this->insure_currency)) ? $this->insure_currency : get_woocommerce_currency();
            $dhl_insurance_at_placing_order = isset($_POST['dhl_insurance'])? $_POST['dhl_insurance']: false;
            if($insurance_enabled == 'yes'){
                if(($insurance_enabled_at_checkout == 'yes') || $dhl_insurance_at_placing_order){
                    $insurance_details = $this->insure_contents ? "<InsuredValue>{$total_insurance_value}</InsuredValue><InsuredCurrency>{$this->insure_currency}</InsuredCurrency>" : "";
                    $additional_insurance_details = ($this->insure_contents && ($this->conversion_rate || $this->insure_converstion_rate)) ? "<QtdShp><QtdShpExChrg><SpecialServiceType>II</SpecialServiceType><LocalSpecialServiceType>XCH</LocalSpecialServiceType></QtdShpExChrg></QtdShp>" : "";
                }
            }
            //insurance value
            update_option("dhl_total_insurance_value", $total_insurance_value);
        }
        
        //If vendor country set, then use vendor address
        if(isset($this->settings['vendor_check']) && ($this->settings['vendor_check'] === 'yes')){
            if(isset($package['origin'])){
                if(isset($package['origin']['country'])){
                    $this->origin_country_1       =     $package['origin']['country'];
                    $this->origin                 =     $package['origin']['postcode'];
                    $this->freight_shipper_city_1 =     $package['origin']['city'];

                    $origin_postcode_city = $this->wf_get_postcode_city($this->origin_country_1, $this->freight_shipper_city_1, $this->origin);

                    $paymentCountryCode = !empty($this->general_settings['dutypayment_country'])? $this->general_settings['dutypayment_country']: $this->general_settings['base_country'];// obtaining payment country code from label settings
                }
            }
        }else{
                $origin_postcode_city = $this->wf_get_postcode_city($this->origin_country, $this->freight_shipper_city_1, $this->origin);
            
                $paymentCountryCode = !empty($this->general_settings['dutypayment_country'])? $this->general_settings['dutypayment_country']: $this->general_settings['base_country'];// obtaining payment country code from label settings
        }

        // For multi-vendor cases
        if(isset($this->settings['vendor_check']) && $this->settings['vendor_check'] === 'yes'){
            $is_dutiable = ($package['destination']['country'] == $this->origin_country_1 || wf_dhl_is_eu_country($this->origin_country_1, $package['destination']['country'])) ? "N" : "Y";
        }else{
            $is_dutiable = ($package['destination']['country'] == $this->origin_country || wf_dhl_is_eu_country($this->origin_country, $package['destination']['country'])) ? "N" : "Y";
        }

        $dutiable_content = $is_dutiable == "Y" ? "<Dutiable><DeclaredCurrency>{$currency}</DeclaredCurrency><DeclaredValue>{$total_value}</DeclaredValue></Dutiable>" : "";

        $destination_city = strtoupper($package['destination']['city']);

        /*There are different country codes for same country from WooCommerce and DHL. Here we are obtaining country code which is mapped to DHL for both source and destination countries*/
        $destination_country_code = $this->get_country_codes_mapped_for_dhl($package['destination']['country']);
        $destination_postcode_city = $this->wf_get_postcode_city($package['destination']['country'], $destination_city, $destination_postcode);
        $source_country_code = $this->get_country_codes_mapped_for_dhl($this->origin_country_1);
        $switch_account_number_action_input = array('account_number' => $this->settings['account_number'], 'source_country_code' => $source_country_code);
        $switch_account_number_action_result = apply_filters('switch_account_number_action', $switch_account_number_action_input, $this->settings['dutypayment_country']);
        $this->account_number = isset($switch_account_number_action_result['payment_account_number'])? $switch_account_number_action_result['payment_account_number']: $switch_account_number_action_result['account_number'];
        $paymentCountryCode = isset($switch_account_number_action_result['payment_country_code'])? $switch_account_number_action_result['payment_country_code']: $switch_account_number_action_result['source_country_code'];
        $fetch_accountrates = $this->request_type == "ACCOUNT" ? "<PaymentAccountNumber>" . $this->account_number . "</PaymentAccountNumber>" : "";

$xmlRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com DCT-req.xsd ">
  <GetQuote>
    <Request>
        <ServiceHeader>
            <MessageTime>{$mailing_datetime}</MessageTime>
            <MessageReference>1234567890123456789012345678901</MessageReference>
            <SiteID>{$this->site_id}</SiteID>
            <Password>{$this->site_password}</Password>
        </ServiceHeader>
    </Request>
    <From>
      <CountryCode>{$source_country_code}</CountryCode>
      {$origin_postcode_city}
    </From>
    <BkgDetails>
      <PaymentCountryCode>{$paymentCountryCode}</PaymentCountryCode>
      <Date>{$mailing_date}</Date>
      <ReadyTime>PT10H21M</ReadyTime>
      <DimensionUnit>{$this->quoteapi_dimension_unit}</DimensionUnit>
      <WeightUnit>{$this->quoteapi_weight_unit}</WeightUnit>
      <Pieces>
        {$pieces}
      </Pieces>
      {$fetch_accountrates}
      <IsDutiable>{$is_dutiable}</IsDutiable>
      <NetworkTypeCode>AL</NetworkTypeCode>
          {$additional_insurance_details}
      {$insurance_details}
      </BkgDetails>
    <To>
      <CountryCode>{$destination_country_code}</CountryCode>
      {$destination_postcode_city}
    </To>
    {$dutiable_content}
  </GetQuote>
</p:DCTRequest>
XML;
        $xmlRequest = apply_filters('wf_dhl_rate_request', $xmlRequest, $package);
        return $xmlRequest;
    }

    private function wf_get_package_piece($dhl_packages) {
        $pieces = "";
        if ($dhl_packages) {
            foreach ($dhl_packages as $key => $parcel) {
                $pack_type = $this->wf_get_pack_type($parcel['packtype']);
                $index = $key + 1;
                $pieces .= '<Piece><PieceID>' . $index . '</PieceID>';
                $pieces .= '<PackageTypeCode>'.$pack_type.'</PackageTypeCode>';
                if($this->packing_method != "weight_based"){
                    if( !empty($parcel['Dimensions']['Height']) && !empty($parcel['Dimensions']['Length']) && !empty($parcel['Dimensions']['Width']) ){
                        $pieces .= '<Height>' . $parcel['Dimensions']['Height'] . '</Height>';
                        $pieces .= '<Depth>' . $parcel['Dimensions']['Length'] . '</Depth>';
                        $pieces .= '<Width>' . $parcel['Dimensions']['Width'] . '</Width>';
                    }
                }
                $package_total_weight   =(string) $parcel['Weight']['Value'];
                $package_total_weight   = str_replace(',','.',$package_total_weight);
                                if($package_total_weight < 0.001){
                                    $package_total_weight = 0.001;
                                }else{
                                    $package_total_weight = round((float)$package_total_weight,3);
                                }
                $pieces .= '<Weight>' . $package_total_weight . '</Weight></Piece>';
            }
        }
        return $pieces;
    }

    private function wf_get_postcode_city($country, $city, $postcode) {
        $no_postcode_country = array('AE', 'AF', 'AG', 'AI', 'AL', 'AN', 'AO', 'AW', 'BB', 'BF', 'BH', 'BI', 'BJ', 'BM', 'BO', 'BS', 'BT', 'BW', 'BZ', 'CD', 'CF', 'CG', 'CI', 'CK',
            'CL', 'CM', 'CO', 'CR', 'CV', 'DJ', 'DM', 'DO', 'EC', 'EG', 'ER', 'ET', 'FJ', 'FK', 'GA', 'GD', 'GH', 'GI', 'GM', 'GN', 'GQ', 'GT', 'GW', 'GY', 'HK', 'HN', 'HT', 'IE', 'IQ', 'IR',
            'JM', 'JO', 'KE', 'KH', 'KI', 'KM', 'KN', 'KP', 'KW', 'KY', 'LA', 'LB', 'LC', 'LK', 'LR', 'LS', 'LY', 'ML', 'MM', 'MO', 'MR', 'MS', 'MT', 'MU', 'MW', 'MZ', 'NA', 'NE', 'NG', 'NI',
            'NP', 'NR', 'NU', 'OM', 'PA', 'PE', 'PF', 'PY', 'QA', 'RW', 'SA', 'SB', 'SC', 'SD', 'SL', 'SN', 'SO', 'SR', 'SS', 'ST', 'SV', 'SY', 'TC', 'TD', 'TG', 'TL', 'TO', 'TT', 'TV', 'TZ',
            'UG', 'UY', 'VC', 'VE', 'VG', 'VN', 'VU', 'WS', 'XA', 'XB', 'XC', 'XE', 'XL', 'XM', 'XN', 'XS', 'YE', 'ZM', 'ZW');

        $postcode_city = "<Postalcode>{$postcode}</Postalcode>";

        $postcode_city = !in_array( $country, $no_postcode_country ) ? $postcode_city : '';
        if( !empty($city) ){
            $postcode_city .= "<City>{$city}</City>";
        }
        return $postcode_city;
    }
    
    /*
    * Access private
    * Function to get country code for corresponding currencies
    * returns array of currencies with countries' codes as values
    */
    public function wf_get_currency_countries() {
        return array(
            'AFN' => array( 'AF' ),
            'ALL' => array( 'AL' ),
            'DZD' => array( 'DZ' ),
            'USD' => array( 'AS', 'IO', 'GU', 'MH', 'FM', 'MP', 'PW', 'PR', 'TC', 'US', 'UM', 'VI' ),
            'EUR' => array( 'AD', 'AT', 'BE', 'CY', 'EE', 'FI', 'FR', 'GF', 'TF', 'DE', 'GR', 'GP', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'MQ', 'YT', 'MC', 'ME', 'NL', 'PT', 'RE', 'PM', 'SM', 'SK', 'SI', 'ES' ),
            'AOA' => array( 'AO' ),
            'XCD' => array( 'AI', 'AQ', 'AG', 'DM', 'GD', 'MS', 'KN', 'LC', 'VC' ),
            'ARS' => array( 'AR' ),
            'AMD' => array( 'AM' ),
            'AWG' => array( 'AW' ),
            'AUD' => array( 'AU', 'CX', 'CC', 'HM', 'KI', 'NR', 'NF', 'TV' ),
            'AZN' => array( 'AZ' ),
            'BSD' => array( 'BS' ),
            'BHD' => array( 'BH' ),
            'BDT' => array( 'BD' ),
            'BBD' => array( 'BB' ),
            'BYR' => array( 'BY' ),
            'BZD' => array( 'BZ' ),
            'XOF' => array( 'BJ', 'BF', 'ML', 'NE', 'SN', 'TG' ),
            'BMD' => array( 'BM' ),
            'BTN' => array( 'BT' ),
            'BOB' => array( 'BO' ),
            'BAM' => array( 'BA' ),
            'BWP' => array( 'BW' ),
            'NOK' => array( 'BV', 'NO', 'SJ' ),
            'BRL' => array( 'BR' ),
            'BND' => array( 'BN' ),
            'BGN' => array( 'BG' ),
            'BIF' => array( 'BI' ),
            'KHR' => array( 'KH' ),
            'XAF' => array( 'CM', 'CF', 'TD', 'CG', 'GQ', 'GA' ),
            'CAD' => array( 'CA' ),
            'CVE' => array( 'CV' ),
            'KYD' => array( 'KY' ),
            'CLP' => array( 'CL' ),
            'CNY' => array( 'CN' ),
            'HKD' => array( 'HK' ),
            'COP' => array( 'CO' ),
            'KMF' => array( 'KM' ),
            'CDF' => array( 'CD' ),
            'NZD' => array( 'CK', 'NZ', 'NU', 'PN', 'TK' ),
            'CRC' => array( 'CR' ),
            'HRK' => array( 'HR' ),
            'CUP' => array( 'CU' ),
            'CZK' => array( 'CZ' ),
            'DKK' => array( 'DK', 'FO', 'GL' ),
            'DJF' => array( 'DJ' ),
            'DOP' => array( 'DO' ),
            'ECS' => array( 'EC' ),
            'EGP' => array( 'EG' ),
            'SVC' => array( 'SV' ),
            'ERN' => array( 'ER' ),
            'ETB' => array( 'ET' ),
            'FKP' => array( 'FK' ),
            'FJD' => array( 'FJ' ),
            'GMD' => array( 'GM' ),
            'GEL' => array( 'GE' ),
            'GHS' => array( 'GH' ),
            'GIP' => array( 'GI' ),
            'QTQ' => array( 'GT' ),
            'GGP' => array( 'GG' ),
            'GNF' => array( 'GN' ),
            'GWP' => array( 'GW' ),
            'GYD' => array( 'GY' ),
            'HTG' => array( 'HT' ),
            'HNL' => array( 'HN' ),
            'HUF' => array( 'HU' ),
            'ISK' => array( 'IS' ),
            'INR' => array( 'IN' ),
            'IDR' => array( 'ID' ),
            'IRR' => array( 'IR' ),
            'IQD' => array( 'IQ' ),
            'GBP' => array( 'IM', 'JE', 'GS', 'GB' ),
            'ILS' => array( 'IL' ),
            'JMD' => array( 'JM' ),
            'JPY' => array( 'JP' ),
            'JOD' => array( 'JO' ),
            'KZT' => array( 'KZ' ),
            'KES' => array( 'KE' ),
            'KPW' => array( 'KP' ),
            'KRW' => array( 'KR' ),
            'KWD' => array( 'KW' ),
            'KGS' => array( 'KG' ),
            'LAK' => array( 'LA' ),
            'LBP' => array( 'LB' ),
            'LSL' => array( 'LS' ),
            'LRD' => array( 'LR' ),
            'LYD' => array( 'LY' ),
            'CHF' => array( 'LI', 'CH' ),
            'MKD' => array( 'MK' ),
            'MGF' => array( 'MG' ),
            'MWK' => array( 'MW' ),
            'MYR' => array( 'MY' ),
            'MVR' => array( 'MV' ),
            'MRO' => array( 'MR' ),
            'MUR' => array( 'MU' ),
            'MXN' => array( 'MX' ),
            'MDL' => array( 'MD' ),
            'MNT' => array( 'MN' ),
            'MAD' => array( 'MA', 'EH' ),
            'MZN' => array( 'MZ' ),
            'MMK' => array( 'MM' ),
            'NAD' => array( 'NA' ),
            'NPR' => array( 'NP' ),
            'ANG' => array( 'AN' ),
            'XPF' => array( 'NC', 'WF' ),
            'NIO' => array( 'NI' ),
            'NGN' => array( 'NG' ),
            'OMR' => array( 'OM' ),
            'PKR' => array( 'PK' ),
            'PAB' => array( 'PA' ),
            'PGK' => array( 'PG' ),
            'PYG' => array( 'PY' ),
            'PEN' => array( 'PE' ),
            'PHP' => array( 'PH' ),
            'PLN' => array( 'PL' ),
            'QAR' => array( 'QA' ),
            'RON' => array( 'RO' ),
            'RUB' => array( 'RU' ),
            'RWF' => array( 'RW' ),
            'SHP' => array( 'SH' ),
            'WST' => array( 'WS' ),
            'STD' => array( 'ST' ),
            'SAR' => array( 'SA' ),
            'RSD' => array( 'RS' ),
            'SCR' => array( 'SC' ),
            'SLL' => array( 'SL' ),
            'SGD' => array( 'SG' ),
            'SBD' => array( 'SB' ),
            'SOS' => array( 'SO' ),
            'ZAR' => array( 'ZA' ),
            'SSP' => array( 'SS' ),
            'LKR' => array( 'LK' ),
            'SDG' => array( 'SD' ),
            'SRD' => array( 'SR' ),
            'SZL' => array( 'SZ' ),
            'SEK' => array( 'SE' ),
            'SYP' => array( 'SY' ),
            'TWD' => array( 'TW' ),
            'TJS' => array( 'TJ' ),
            'TZS' => array( 'TZ' ),
            'THB' => array( 'TH' ),
            'TOP' => array( 'TO' ),
            'TTD' => array( 'TT' ),
            'TND' => array( 'TN' ),
            'TRY' => array( 'TR' ),
            'TMT' => array( 'TM' ),
            'UGX' => array( 'UG' ),
            'UAH' => array( 'UA' ),
            'AED' => array( 'AE' ),
            'UYU' => array( 'UY' ),
            'UZS' => array( 'UZ' ),
            'VUV' => array( 'VU' ),
            'VEF' => array( 'VE' ),
            'VND' => array( 'VN' ),
            'YER' => array( 'YE' ),
            'ZMW' => array( 'ZM' ),
            'ZWD' => array( 'ZW' ),
        );
    }
    
    

    private function wf_get_package_total_value($dhl_packages) {
        $total_value = 0;
        if ($dhl_packages) {
            foreach ($dhl_packages as $key => $parcel) {
                $total_value += $parcel['InsuredValue']['Amount'] * $parcel['GroupPackageCount'];
            }
        }
        return $total_value;
    }

    public function calculate_shipping( $packages=array() ) {
        global $woocommerce;
        // Clear rates
        $this->found_rates = array();
        
        // Debugging
        $this->debug(__('dhl debug mode is on - to hide these messages, turn debug mode off in the settings.', 'wf-shipping-dhl'));
        // Packages returned ahould be an array regardless of filter added or not 
        $parcels = apply_filters('wf_filter_package_address', array($packages) , $this->ship_from_address);
        add_option("shop_page", 'no');
        add_option("checkout_page", 'no');
        if(get_option('current_order_data') != ''){
            delete_option('current_order_data');
        }


        // Get requests
        $dhl_requests   =   array();
        $dhl_insurance = false;
        foreach($parcels as $parcel){
            $dhl_packs      =   $this->get_dhl_packages( $parcel );
            $str = "";
            $http_referer = "";
            $req_uri = "";
            if($_SERVER){
                if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
                    $http_referer = $_SERVER['HTTP_REFERER'];
                    $req_uri = $_SERVER['REQUEST_URI'];
                }
            }
            
            $this->http_req_referer = $http_referer;
            
            if(isset($_POST['post_data'])){
                parse_str($_POST['post_data'],$str);
            }
            
            /*Handling insurance charge on checkout page*/
            if(strpos($http_referer, 'checkout') > 0){
                if($this->settings['insure_contents'] == 'yes'){
                    update_option('dhl_insurance', 'yes');
                    if($this->settings['insure_contents_chk'] == 'no'){
                        update_option("insure contents check", 'no');
                        $parcel['dhl_insurance'] = true;   
                    }else{
                        /*If DHL insurance is enabled in the Check out field*/
                        if(isset($str['dhl_insurance'])){
                            update_option('dhl_insurance_at_checkout', 'yes');
                            $parcel['dhl_insurance'] = true;
                            $insurance_enabled_at_checkout = 'yes';
                        }else{
                            $insurance_enabled_at_checkout = 'no';
                            $parcel['dhl_insurance'] = false;
                            update_option('dhl_insurance_at_checkout', 'no');
                        }
                    }
                }else{
                    update_option('dhl_insurance', 'no');
                }
            }

            /*Handling insurance charge on shop page*/
            if(is_shop()){
                if($this->settings['insure_contents'] == 'yes'){
                    update_option('dhl_insurance', 'yes');
                    $parcel['dhl_insurance'] = true;
                }else{
                    update_option('dhl_insurance', 'no');
                }
            }
            
            /*Handling insurance charge on cart page*/
            if(is_cart()){
                if($this->settings['insure_contents'] == 'yes'){
                    update_option('dhl_insurance', 'yes');
                    $parcel['dhl_insurance'] = true;
                }else{
                    update_option('dhl_insurance', 'no');
                }
            }
            
            /*Handling insurance charge while adding items to cart*/
            if(strpos($req_uri, 'add_to_cart') > 0){
                if($this->settings['insure_contents'] == 'yes'){
                    update_option('dhl_insurance', 'yes');
                    $parcel['dhl_insurance'] = true;
                }else{
                    update_option('dhl_insurance', 'no');
                }  
            }
            
            $dhl_reqs       = $this->get_dhl_requests( $dhl_packs, $parcel );
            $dhl_insurance  = isset($parcel['dhl_insurance']) && ($parcel['dhl_insurance'] == true)? true : false;
            $dhl_requests[] = $dhl_reqs;
        }
        if ($dhl_requests) {
            $this->run_package_request($dhl_requests,$dhl_insurance);
        }


        // Ensure rates were found for all packages
        $packages_to_quote_count = sizeof($dhl_requests);
        
        if ($this->found_rates) {
            foreach ($this->found_rates as $key => $value) {
                if ($value['packages'] < $packages_to_quote_count) {
                    unset($this->found_rates[$key]);
                }
            }
        }
        // Rate conversion
        if ($this->conversion_rate) {
            foreach ($this->found_rates as $key => $rate) {
                $this->found_rates[$key]['cost'] = $rate['cost'] * $this->conversion_rate;
            }
        }
        $this->add_found_rates();
       
    }

    public function run_package_request($requests,$dhl_insurance) {
        try {
            foreach ( $requests as $key => $request ) {
                $this->process_result($this->get_result($request), $request, $dhl_insurance);
            }            
        } catch (Exception $e) {
            $this->debug(print_r($e, true), 'error');
            return false;
        }
    }

    private function get_result($request) {

        $this->debug('DHL REQUEST: <a href="#" class="debug_reveal">Reveal</a><pre class="debug_info" style="background:#3d9cd2;border:1px solid #DDD;padding:5px;color:white">' . print_r(htmlspecialchars($request), true) . '</pre>');


        $result = wp_remote_post($this->service_url, array(
            'method' => 'POST',
            'timeout' => 70,
            'sslverify' => 0,
            //'headers'          => $this->wf_get_request_header('application/vnd.cpc.shipment-v7+xml','application/vnd.cpc.shipment-v7+xml'),
            'body' => $request
                )
        );

        wc_enqueue_js("
            jQuery('a.debug_reveal').on('click', function(){
                jQuery(this).closest('div').find('.debug_info').slideDown();
                jQuery(this).remove();
                return false;
            });
            jQuery('pre.debug_info').hide();
        ");

        if ( is_wp_error( $result ) ) {
            $error_message = $result->get_error_message();
            $this->debug('DHL WP ERROR: <a href="#" class="debug_reveal">Reveal</a><pre class="debug_info" style="background:red;border:1px solid #DDD;padding:5px;">' . print_r(htmlspecialchars($error_message), true) . '</pre>');
        }
        elseif (is_array($result) && !empty($result['body'])) {
            $result = $result['body'];
        } else {
            $result = '';
        }
        if(!empty($result) && is_string($result))
        {
            $this->debug('DHL RESPONSE: <a href="#" class="debug_reveal">Reveal</a><pre class="debug_info" style="background:#3d9cd2;border:1px solid #DDD;padding:5px;color:white">' . print_r(htmlspecialchars($result), true) . '</pre>');
        }
        libxml_use_internal_errors(true);
        if(!empty($result) && is_string($result))
        {
            $xml = simplexml_load_string(utf8_encode($result));
        }
        if ($xml) {
            return $xml;
        } else {
            return null;
        }
    }

    private function wf_get_dhl_base_currency(){
        $base_country = $this->general_settings['base_country'];
        $currency_countries = array();
        $currency_countries = $this->wf_get_currency_countries();
        $base_currency = '';
        //Obtaing base country currency code for provided base country code
        foreach($currency_countries as $currency=>$countries){
            foreach($countries as $country){
                if($country == $base_country){
                    $base_currency = $currency;
                }
            }
        }

        return $base_currency;
    }

    public function wf_get_currency_based_on_country_code($country_code){
        $currency_countries = array();
        $currency_countries = $this->wf_get_currency_countries();
        $currency_code = '';
        //Obtaing currency code for provided country code
        foreach($currency_countries as $currency=>$countries){
            foreach($countries as $country){
                if($country == $country_code){
                    $currency_code = $currency;
                }
            }
        }

        return $currency_code;
    }

    private function wf_get_cost_based_on_currency($qtdsinadcur, $default_charge, $charge_type) {
        $base_currency = $this->wf_get_dhl_base_currency();
        
        if (!empty($qtdsinadcur)) {
            foreach ($qtdsinadcur as $multiple_currencies) {
                if($charge_type == "shipping"){
                    if ((string) $multiple_currencies->CurrencyCode == $base_currency && !empty($multiple_currencies->TotalAmount) && ($multiple_currencies->TotalAmount != 0)){
                        return $multiple_currencies->TotalAmount;   
                     }
                }else{
                    if ((string) $multiple_currencies->CurrencyCode == $base_currency && !empty($multiple_currencies->WeightCharge) && ($multiple_currencies->WeightCharge != 0)){
                        return $multiple_currencies->WeightCharge;   
                     }
                }
                //else if ((string) $multiple_currencies->CurrencyCode == get_woocommerce_currency() && !empty($multiple_currencies->TotalAmount)){
                //    return $multiple_currencies->TotalAmount;
                // }
            }
        }
        return $default_charge;
    }

    private function process_result($result,$defined_req = array(), $dhl_inurance) {// = false
        
        $processed_ratecode = array();
        $rate_compain = '';
        $rate_local_code ='';
        
        $base_currency = $this->wf_get_dhl_base_currency();
        
        if ($result && !empty($result->GetQuoteResponse->BkgDetails->QtdShp)) {
            foreach ($result->GetQuoteResponse->BkgDetails->QtdShp as $quote) {
                $rate_code = strval((string) $quote->GlobalProductCode);
                $rate_local_code = strval((string) (isset($quote->LocalProductCode) ? $quote->LocalProductCode : ''));
                if (!in_array($rate_code,$processed_ratecode)) {
//                  $shipping_rates_source_currency = apply_filters('wf_dhl_shipping_rates_source_currency', get_woocommerce_currency(), $result, $this);
                    $shipping_rates_source_currency = $base_currency;
                    if ((string) $quote->CurrencyCode == $shipping_rates_source_currency) {
                        $rate_cost = floatval((string) $quote->ShippingCharge);
                        $rate_compain = floatval((string) $quote->WeightCharge);
                    }else{
                        $charge_type = "shipping";
                        $rate_cost = floatval((string) $this->wf_get_cost_based_on_currency($quote->QtdSInAdCur, $quote->ShippingCharge, $charge_type));
                        $charge_type = "weight";
                        $rate_compain = floatval((string) $this->wf_get_cost_based_on_currency($quote->QtdSInAdCur, $quote->WeightCharge, $charge_type));
                    }
                    $processed_ratecode[] = $rate_code;
                    $rate_id = $this->id . ':' . $rate_code.'|'.$rate_local_code;
                    
                    $delivery_time = new DateInterval($quote->DeliveryTime);
                    $delivery_time = $delivery_time->format('%h:%I');
                    $delivery_date_time = date("M-d", strtotime($quote->DeliveryDate)).' '.$delivery_time;
                    $rate_name = strval( (string) $quote->ProductShortName );
                    if($rate_cost > 0) $this->prepare_rate($rate_code, $rate_id, $rate_name, $rate_cost,$delivery_date_time,$rate_compain,$dhl_inurance,$shipping_rates_source_currency);
                }
            }
        }
    }

    private function prepare_rate($rate_code, $rate_id, $rate_name, $rate_cost, $delivery_time, $rate_compain = '0',$dhl_insurance,$shipping_rates_source_currency) {
        $default_currency = $this->wf_get_currency_based_on_country_code(WC()->countries->get_base_country());

        // Name adjustment
        if (!empty($this->custom_services[$rate_code]['name'])) {
            $rate_name = $this->custom_services[$rate_code]['name'];
        }

        // Cost adjustment %
        if (!empty($this->custom_services[$rate_code]['adjustment_percent'])) {
            $rate_cost = $rate_cost + ( $rate_cost * ( floatval($this->custom_services[$rate_code]['adjustment_percent']) / 100 ) );
        }
        // Cost adjustment
        if (!empty($this->custom_services[$rate_code]['adjustment'])) {
            $rate_cost = $rate_cost + floatval($this->custom_services[$rate_code]['adjustment']);
        }

        // Enabled check
        if (isset($this->custom_services[$rate_code]) && empty($this->custom_services[$rate_code]['enabled'])) {
            return;
        }

        // Merging
        if (isset($this->found_rates[$rate_id])) {
            $rate_cost = $rate_cost + $this->found_rates[$rate_id]['cost'];
            $packages = 1 + $this->found_rates[$rate_id]['packages'];
        } else {
            $packages = 1;
        }

        // Sort
        if (isset($this->custom_services[$rate_code]['order'])) {
            $sort = $this->custom_services[$rate_code]['order'];
        } else {
            $sort = 999;
        }
        
        $extra_charge = $rate_cost - $rate_compain;
        $insurance_charge = 0;

        if ($this->conversion_rate) {
                $extra_charge = $extra_charge * $this->conversion_rate;
                $rate_compain = $rate_compain * $this->conversion_rate;
        }

        if($this->aelia_activated){
            $rate_compain = apply_filters('wc_aelia_cs_convert', $rate_compain, $default_currency, get_woocommerce_currency()) != ''? apply_filters('wc_aelia_cs_convert', $rate_compain, $default_currency, get_woocommerce_currency()) : '';
            $extra_charge = apply_filters('wc_aelia_cs_convert', $extra_charge, $default_currency, get_woocommerce_currency()) != ''? apply_filters('wc_aelia_cs_convert', $extra_charge, $default_currency, get_woocommerce_currency()) : '';
            // $insurance_charge = apply_filters('wc_aelia_cs_convert', $insurance_charge, $default_currency, get_woocommerce_currency()) != ''? apply_filters('wc_aelia_cs_convert', $insurance_charge, $default_currency, get_woocommerce_currency()) : '';
        }

        //Will implement in future versions
        // if($this->general_settings['show_dhl_insurance_charges'] == 'yes'){
        //     $insurance_charge = get_option('dhl_total_insurance_value');
        // }

        try{
            $this->found_rates[$rate_id] = apply_filters('wf_dhl_shipping_found_rate' , array(
                'id' => $rate_id,
                'label' => $rate_name,
                'cost' => $rate_cost,
                'sort' => $sort,
                'packages' => $packages,
                'meta_data' => array('dhl_delivery_time'=>$delivery_time,'weight_charge'=>floatval($rate_compain),'extra_charge'=>$extra_charge,'insurance' => (($dhl_insurance) ? 'yes' : 'no'), 'insurance_charge' => ($dhl_insurance? $insurance_charge: ''))
            ),$shipping_rates_source_currency, $this);
        }catch(Error $e){
            if(is_plugin_active('woocommerce-aelia-currencyswitcher/woocommerce-aelia-currencyswitcher.php')){
                $this->debug("Provide proper exchange rate");
            }else{
                $this->debug(print_r($e));
            }
        }
    }

    public function add_found_rates() {
        if ($this->found_rates) {

            if ($this->offer_rates == 'all') {

                uasort($this->found_rates, array($this, 'sort_rates'));

                foreach ($this->found_rates as $key => $rate) {
                    $this->add_rate($rate);
                }
            } else {
                $cheapest_rate = '';

                foreach ($this->found_rates as $key => $rate) {
                    if (!$cheapest_rate || $cheapest_rate['cost'] > $rate['cost']) {
                        $cheapest_rate = $rate;
                    }
                }

                $cheapest_rate['label'] = $this->title;

                $this->add_rate($cheapest_rate);
            }
        }
    }

    public function sort_rates($a, $b) {
        if ($a['sort'] == $b['sort'])
            return 0;
        return ( $a['sort'] < $b['sort'] ) ? -1 : 1;
    }
    private function wf_get_pack_type($selected) {
            $pack_type = 'BOX';
            if ($selected == 'FLY') {
                $pack_type = 'FLY';
            } 
        return $pack_type;    
    }

}

