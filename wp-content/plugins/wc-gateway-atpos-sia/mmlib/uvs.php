<?php
/*
 *  === Mauro Mascia Update Library ===
 *
 * Version: 1.0
 * Author: Mauro Mascia
 * Author URI: http://www.mauromascia.com
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Support: info@mauromascia.com
 * Copyright: © 2013-2015 Mauro Mascia
 *
 * Thanks to:
 * @ Jeremy Clark (clark-technet.com) https://github.com/jeremyclark13/automatic-theme-plugin-update
 * @ http://wp.tutsplus.com/tutorials/plugins/a-guide-to-the-wordpress-http-api-automatic-plugin-updates/
 * @ The book "Professional WordPress Plugin Development" by Brad Williams, Ozh Richard, Justin Tadlock
 * @ WooThemes - WooCommerce Software Add-on (http://www.woothemes.com/products/software-add-on/)
 *
 * Side notes
 * http://www.gnu.org/licenses/gpl-faq.html#GPLCommercially

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'MMascia_Update_Validation_System' ) ) :

class MMascia_Update_Validation_System {

  public function __construct( $args, $debug = false ) {
    $this->plugin_slug         = $args['slug'];
    $this->plugin_slug_us      = str_replace( "-", "_", $this->plugin_slug );
    $this->plugin_path         = $args['plugin_path'];
    $this->plugin_url          = $args['plugin_url'];
    $this->updater_url         = $args['updater_url'];
    $this->api_url             = $args['api_url'];
    $this->plugin_info_file    = $this->plugin_slug .'/'. $this->plugin_slug .'.php';
    $this->instance            = md5( get_bloginfo( 'url' ) );
    $this->debug               = $debug;

    // stored options
    $this->wp_option_license_key   = $this->plugin_slug_us."_license_key";
    $this->wp_option_email         = $this->plugin_slug_us."_email";

    load_plugin_textdomain( 'uvs', false, dirname( plugin_basename( __FILE__ ) ) . "/lang" );

    $this->strings = array(
      'page_head' => __( '' ),
      'page_content' => __( 'Enter your email address used during the purchase and the license key you received after the purchase, and then click the Activate button.', 'uvs' )
    );

    // Set and verify if the option is already present
    if ( get_option( $this->wp_option_license_key, false ) == false ) {
      add_option( $this->wp_option_license_key, '' );
    }
    $this->license = get_option( $this->wp_option_license_key );

    if ( get_option( $this->wp_option_email, false ) == false ) {
      add_option( $this->wp_option_email, '' );
    }
    $this->email = get_option( $this->wp_option_email );

    if ( ! empty( $this->license ) && ! empty( $this->email ) ) {
      // These args are used from the updater to validate the request
      $this->validation_args = array(
        'licence_key'   => $this->license,
        'email'         => $this->email,
        'product_id'    => $this->plugin_slug,
        'instance'      => $this->instance
      );

      add_filter( 'pre_set_site_transient_update_plugins', array( &$this, 'check_for_plugin_update' ) );
      add_filter( 'plugins_api', array( &$this, 'plugin_api_call' ), 10, 3 );
    }

    // add the license key link
    add_action( 'plugin_action_links_'. $this->plugin_path, array( &$this, 'plugin_action_links' ) );

    // view the license page when the above link is clicked
    add_action( 'wp_ajax_'.$this->plugin_slug.'_license', array( &$this, 'view_license' ) );
  }

  function do_action( $action, $echo = false ) {
    // Required
    if ( ! class_exists( 'MMascia_Update_Validation_System_Requests' ) ) {
      require_once dirname(__FILE__) . '/uvs-requests.php';
    }

    // Create a new request object - @see view_license()
    $requests = new MMascia_Update_Validation_System_Requests( $this->api_url, $this->debug );
    $response = $requests->do_action( $action, $this->validation_args, $echo );
    $this->uvs_log( var_export($response,true) );
    $this->uvs_log( $action . ': ' . ( $response ? 'OK' : 'KO' ), "[REQUEST STATUS]" );

    return $response;
  }

  // Take over the update check
  function check_for_plugin_update( $transient ) {

    // Check if the transient contains the 'checked' information
    // If no, just return its value without hacking it
    if ( empty( $transient->checked ) || !isset( $transient->checked[$this->plugin_info_file] ) ) {
      return $transient;
    }

    // WARNING - Removing the if line does not allow you to be a smart n00b and receive
    // updates. It will be verified server-side, anyway ;)
    // It's here to not send unuseful requests.
    if ( ! $this->do_action( 'is_active' ) ) {
      return $transient;
    }

    global $wp_version;

    // The transient contains the 'checked' information, add this info for the API
    $args = array(
      'slug'     => $this->plugin_slug,
      'version'  => $transient->checked[$this->plugin_info_file],
    );

    $request_string = array(
      'body' => array(
        'action'       => 'basic_check',
        'request'      => serialize( $args ),
        'api-key'      => md5( get_bloginfo( 'url' ) ), //instance
        'validation'   => serialize( $this->validation_args )
      ),
      'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' )
    );

    // Start checking for an update
    $raw_response = wp_remote_post( $this->updater_url, $request_string );
    $this->uvs_log( "UVS wp_remote_post to: " . $this->updater_url );

    if ( ! is_wp_error( $raw_response ) && isset($raw_response['body']) && isset($raw_response['response']) 
      && isset($raw_response['response']['code']) && $raw_response['response']['code'] == 200 ) {
      $response = unserialize( $raw_response['body'] );
    }
    else {
      $this->uvs_log( "Response code: " . var_export( $raw_response, true ) );
    }

    // Feed the update data into WP updater
    if ( is_object( $response ) && !empty( $response ) ) {
      $transient->response[$this->plugin_info_file] = $response;
    }

    return $transient;
  }

  // Take over the Plugin info screen
  function plugin_api_call( $original, $action, $args ) {
    global $wp_version;

    if ( $args->slug != $this->plugin_slug )
      return $original;

    // Get the current version
    $transient = get_site_transient( 'update_plugins' );
    $args->version = $transient->checked[$this->plugin_info_file];

    $request_string = array(
      'body' => array(
        'action'      => $action,
        'request'     => serialize( $args ),
        'api-key'     => $this->instance,
        'validation'  => serialize( $this->validation_args )
      ),
      'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' )
    );

    $response = wp_remote_post( $this->updater_url, $request_string );

    if ( is_wp_error( $response ) ) {
      $res = new WP_Error( 'plugins_api_failed', __( 'An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>' ), $request->get_error_message() );
    }
    else {
      $res = unserialize( $response['body'] );

      $this->uvs_log( "doing plugin_api_call slug: '" . $args->slug . "' - action: " . $action, "[NOTICE]" );

      if ( $res === false )
        $res = new WP_Error( 'plugins_api_failed', __( 'An unknown error occurred' ), $response['body'] );
    }

    return $res;
  }

  public function ajax_url() {
    return esc_url(
      add_query_arg( 
        array(
          'slug'          => $this->plugin_slug,
          'action'        => $this->plugin_slug . '_license',
          '_ajax_nonce'   => wp_create_nonce( $this->plugin_slug . '_license' ),
          'TB_iframe'     => true,
          // 'tab'           => 'plugin-information',
          // 'width'         => 600,
          // 'height'        => 550
        ),
        admin_url( 'admin-ajax.php' )
      )
    );
  }

  /**
   * Add the license link to the $action array
   * @see wp-admin/includes/class-wp-plugins-list-table.php
   *
   * @param array $actions
   * @return array
   */
  public function plugin_action_links( $actions ) {
    $key = $this->plugin_url . basename( dirname( __FILE__ ) ) .'/key.png';
    $img = ' <img src="' . $key . '" style="float:none!important; width:16px!important;height:16px!important;" />';
		$actions[sizeof( $actions )] = '<a href="' . $this->ajax_url() . '" class="thickbox">ABILITA AGGIORNAMENTI AUTOMATICI ' . $img . '</a>';
    return $actions;
	}

  private function get_plugin_options() {
    if ( is_multisite() ) {
      $options = get_site_option( $this->plugin_slug . '_options', false, false );
    }
    else {
      $options = get_option( $this->plugin_slug . '_options' );
    }
    return ( ! $options || ! is_array( $options ) ) ? array() : $options;
  }

  private function save_plugin_options( $clearhash = false) {
    $options = $this->get_plugin_options();

    $options[ $this->plugin_slug ] = $this->plugins[ $this->plugin_slug ];
    if ( !empty( $this->plugins[ 'userhash' ] ) ) $options[ 'userhash' ] = $this->plugins[ 'userhash' ];
    if ( !empty( $this->plugins[ 'username' ] ) ) $options[ 'username' ] = $this->plugins[ 'username' ];
    if ( $clearhash == true ) {
      $this->plugins[ 'userhash' ] = $options[ 'userhash' ] = '';
      $this->plugins[ 'username' ] = $options[ 'username' ] = '';
    }

    if ( is_multisite() ) {
      $this->update_site_option( $this->plugin_slug.'_options', $options );
    }
    else {
      $this->update_option( $this->plugin_slug.'_options', $options );
    }
  }

  public function view_license() {
    check_ajax_referer( $this->plugin_slug.'_license', '_ajax_nonce' );
    require_once dirname(__FILE__) . '/uvs-license-page.php';
    die();
  }

  public function uvs_log( $error = '', $type = '[NOTICE]' ) {
    if ( $this->debug ) {
      error_log( "[MMASCIA_UVS]$type $error\n" );
    }
  }

}// end class

endif;

?>