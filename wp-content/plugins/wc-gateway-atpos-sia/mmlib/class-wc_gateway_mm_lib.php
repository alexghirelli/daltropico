<?php
/**
 * === Mauro Mascia WooCommerce Library ===
 *
 * Version: 20190707
 * Author: Mauro Mascia
 * Author URI: http://www.mauromascia.com
 * Support: info@mauromascia.com
 *
 * Copyright: © 2013-2019 MAURO MASCIA
 *
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 */

// hide plugin https://trickspanda.com/hide-wordpress-plugin-plugin-list/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WC_Gateway_MM_Lib' ) ) :

class WC_Gateway_MM_Lib {

  public $plugin_url;
  public $plugin_path;
  public $plugin_slug;
  public $plugin_textdomain;
  public $plugin_logfile;
  private $file;

  function __construct( $file = null ) {
    if ( null == $file ) {
      echo '<div id="message" class="error woocommerce-error"><p>FATAL ERROR: WC_Gateway_MM_Lib requires the main plugin file!</p></div>';
      return;
    }

    $this->plugin_main_file      = $file;
    $this->plugin_url            = trailingslashit( plugins_url( '', $plugin = $file ) );
    $this->plugin_dir_path       = plugin_dir_path( $file );
    $this->plugin_path           = dirname( plugin_basename( $file ) );
    $this->plugin_slug           = basename( $file, '.php' );
    $this->plugin_slug_dashed    = str_replace( "-", "_", $this->plugin_slug );
    $this->plugin_textdomain     = $this->plugin_slug;
    $this->plugin_logfile_name   = $this->get_plugin_logfile_name();
    $this->mmlogo                = $this->plugin_url.basename(dirname( __FILE__ )).'/mm-logo.png';
    $this->form_fields_file      = dirname( $this->plugin_main_file ) . '/inc/init_form_fields.php';
  }

  /**
   * Localize, script and init the gateway
   */
  function __init_gateway( &$this_gw ) {
    $this->gw = $this_gw;

    // Localize
    load_plugin_textdomain( $this->plugin_slug, false, $this->plugin_path . "/languages" );
    load_plugin_textdomain( 'mmlib', false, $this->plugin_path . "/mmlib/lang" );

    // Style
    wp_register_style( $this->plugin_slug.'-css', $this->plugin_url.'/'.$this->plugin_slug.'.css' );
    wp_enqueue_style( $this->plugin_slug.'-css' );

    // Maybe load the strings used on this plugin
    if ( method_exists( $this_gw, 'init_strings' ) ) {
      $this_gw->init_strings();
    }

    // Load form fields and settings
    $this_gw->form_fields = file_exists( $this->form_fields_file ) ? include $this->form_fields_file : $this_gw->init_form_fields();
    $this_gw->init_settings();
    $this->load_card_icons();
  }


  function set_gw( &$this_gw ) {
    $this->send_debug_email = isset( $this_gw->send_debug_email ) && $this_gw->send_debug_email ? true : false;

    // Maybe check if this gateway is valid for use.
    if ( method_exists( $this_gw, 'is_valid_for_use' ) && ! $this_gw->is_valid_for_use() ) {
      $this_gw->enabled = 'no';
    }
  }

  function get_single_card_settings_array( $name ) {

    return array(
        'title' => '',
        'type' => 'checkbox',
        'label' => $name,
        'default' => 'no'
    );
  }

  function get_cards_settings() {
    return array(
      // -- ICONS
      'cards' => array(
        'title' => __( 'Overwrite Card Icons', 'mmlib' ),
        'type' => 'title',
        'description' => __( 'Select the accepted cards to show them as icon', 'mmlib' ),
        'class' => 'mmnomargin',
      ),
      'card_visa'       => $this->get_single_card_settings_array( 'Visa Electron' ),
      'card_mastercard' => $this->get_single_card_settings_array( 'Mastercard' ),
      'card_maestro'    => $this->get_single_card_settings_array( 'Maestro' ),
      'card_ae'         => $this->get_single_card_settings_array( 'American Express' ),
      'card_dci'        => $this->get_single_card_settings_array( 'Diners Club International' ),
      'card_paypal'     => $this->get_single_card_settings_array( 'PayPal' ),
      'card_jcb'        => $this->get_single_card_settings_array( 'JCB Cards' ),
      'card_postepay'   => $this->get_single_card_settings_array( 'PostePay' ),
    );
  }

  /**
   * Load card icons.
   */
  function load_card_icons() {
    $cards = array();
    $card_path = $this->plugin_url . basename( dirname( __FILE__ ) ) . '/cards/';

    $card_settings = $this->get_cards_settings();
    foreach ( $card_settings as $card => $setting ) {
      if ( !empty( $this->gw->settings[$card] ) && $this->gw->settings[$card] == "yes" ) {
        $cards[] = $card_path . $card . '.jpg';
      }
    }

    if ( empty( $cards ) ) return;

    // workaround for get_icon() of WC_Payment_Gateway
    // @see abstract-wc-payment-gateway.php
    $cards_string = '';
    foreach ( $cards as $card ) {
      $cards_string .= $card . ( end( $cards ) == $card ? '' : '" /><img src="' );
    }

    $this->gw->icon = $cards_string;
  }


  function get_plugin_logfile_name() {
    return (defined( 'WC_LOG_DIR' ) ? WC_LOG_DIR : '' ) .$this->plugin_slug."-".sanitize_file_name( wp_hash( $this->plugin_slug ) ).'.log';
  }


  function log_add( $message ) {
    if ( $this->gw->debug ) {
      if ( ! isset( $this->log ) || empty( $this->log ) ) {
        $this->log = new WC_Logger();
      }
      $this->log->add( $this->plugin_slug, $message );
    }
  }


  // clean and validate order's prefix
  function get_order_prefix( &$settings ) {
    if ( isset( $settings['order_prefix'] ) && ! empty( $settings['order_prefix'] ) ) {
      // allows only alphanumeric charactes
      $prefix = preg_replace( "/[^A-Za-z0-9]/", '', $settings['order_prefix'] );

      // max 15 char
      $prefix = substr( $prefix, 0, 15 );

      // Update the order prefix value
      $settings['order_prefix'] = $prefix;

      return $prefix;
    }

    return '';
  }

  function wc_url( $order, $path ) {
      switch ( $path ) {
          case 'view_order':
              return $order->get_view_order_url();

          case 'order_received':
              $url = $this->gw->get_return_url( $order );
              $url = $this->adjust_url_lang( $url );
              return add_query_arg( 'utm_nooverride', '1',  $url );

          case 'order_failed':
              $url = wc_get_checkout_url();
              $url = $this->adjust_url_lang( $url );
              return add_query_arg( 'utm_nooverride', '1',  $url );

          case 'pay':
              return $order->get_checkout_payment_url( true );
      }

      return '';
  }


  /**
   * Update order status, add admin order note and empty the cart
   */
  function wc_order_completed( $order, $message, $tx_id='' ) {
      $order->payment_complete( $tx_id );
      $order->add_order_note( $message );
      WC()->cart->empty_cart();

      $this->log_add( 'ORDER COMPLETED: ' . $message );

      // FIX: under some circustances emails seems to not be fired. This force them to be sent.
      if ( defined( 'WC_GATEWAY_FORCE_SEND_EMAIL' ) && WC_GATEWAY_FORCE_SEND_EMAIL ) {
          $mailer = WC()->mailer();
          $mails = $mailer->get_emails();
          if ( ! empty( $mails ) ) {
              foreach ( $mails as $mail ) {
                  if ( ( $order->has_status( 'completed' ) && ($mail->id == 'customer_completed_order' || $mail->id == 'new_order') )
                      || ( $order->has_status( 'processing' ) && ($mail->id == 'customer_processing_order' || $mail->id == 'new_order') ) ) {
                      $mail->trigger( $order->get_id() );
                }
              }
          }
      }
      // \FIX
  }

  /**
   * Create the gateway form, loading the autosubmit javascript.
   */
  function get_gw_form( $action_url, $input_params, $order ) {

    $assets_path = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';
    $imgloader = $assets_path . 'images/ajax-loader@2x.gif';
    $js = <<<JS
        jQuery('html').block({
            message: '<img src="$imgloader" alt="Redirecting&hellip;" style="float:left;margin-right:10px;"/>Thank you! We are redirecting you to make payment.',
            overlayCSS: {
                background: '#fff',
                opacity: 0.6
            },
            css: {
                padding: 20,
                textAlign: 'center',
                color: '#555',
                border: '3px solid #aaa',
                backgroundColor: '#fff',
                cursor: 'wait',
                lineHeight: '32px'
            }
        });
        jQuery('#submit__{$this->plugin_slug_dashed}').click();
JS;

    wc_enqueue_js( $js );

    $action_url        = esc_url_raw( $action_url );
    $cancel_url        = esc_url_raw( $order->get_cancel_order_url() );
    $pay_order_str     = 'Pay via '.$this->gw->method_title;
    $cancel_order_str  = 'Cancel order &amp; restore cart';

    $input_fields = "";
    foreach ( $input_params as $key => $value ) {
        $input_fields.= '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $value ) . '" />';
    }

    return <<<HTML
        <form action="{$action_url}" method="POST" id="form__{$this->plugin_slug_dashed}" target="_top">
            $input_fields
            <input type="submit" class="button-alt" id="submit__{$this->plugin_slug_dashed}" value="{$pay_order_str}" />
            <a class="button cancel" href="$cancel_url">{$cancel_order_str}</a>
        </form>
HTML;
  }

  /**
   * Backwards compatible add error
   */
  function wc_add_error( $error ) {
      if ( function_exists( 'wc_add_notice' ) ) {
        wc_add_notice( $error, 'error' );
      }
  }

  /**
   * Check if qTranslate-X or mqTranslate is enabled.
   *
   * @return bool true if one of them is active, false otherwise.
   */
  function is_qtranslate_enabled() {
    return ( defined('QTX_VERSION') ||
      in_array( 'qtranslate/qtranslate.php',   (array) get_option( 'active_plugins', array() ) ) ||
        in_array( 'mqtranslate/mqtranslate.php', (array) get_option( 'active_plugins', array() ) ) );
  }

  /**
   * Checks if WooCommerce Subscriptions is active
   *
   * @return bool true if WCS is active, false otherwise.
   */
  function is_subscriptions_active() {
    return in_array( 'woocommerce-subscriptions/woocommerce-subscriptions.php', (array) get_option( 'active_plugins', array() ) );
  }


  /**
   * Returns current language checking for qTranslate|mqTranslate|WPML
   * Fallback on get_locale() if nothing found.
   * @return string
   */
  function get_current_language() {
    if ( $this->is_qtranslate_enabled() ) {
      if ( function_exists( 'qtranxf_getLanguage' ) ) {
        return qtranxf_getLanguage(); // -- qTranslate X
      }
      else if ( function_exists( 'qtrans_getLanguage' ) ) {
        return qtrans_getLanguage(); // -- qTranslate / mqTranslate
      }
    }
    elseif ( defined( 'ICL_LANGUAGE_CODE' ) ) { // --- Wpml
      return ICL_LANGUAGE_CODE;
    }
    return get_locale();
  }

  /**
   * Returns the two characters of the language in lowercase
   * @return string
   */
  function get_current_language_2dgtlwr() {
    return substr( strtolower( $this->get_current_language() ), 0, 2 );
  }

  /**
   * Adjust the URL (pre-path, pre-domain or query)
   * ATM only qTranslate/mqtranslate is supported
   */
  function adjust_url_lang( $url ) {
    if ( $this->is_qtranslate_enabled() && function_exists( 'qtrans_convertURL' ) ) {
      return qtrans_convertURL( $url, $this->get_current_language_2dgtlwr() );
    }
    return $url;
  }


  // Generate the option list
  function get_page_list_as_option() {
    $opt_pages = array( 0 => " -- Select -- " );
    foreach ( get_pages() as $page ) {
      $opt_pages[ $page->ID ] = __( $page->post_title );
    }
    return $opt_pages;
  }

  /**
   * Mostra un messaggio d'errore.
   */
  function show_error( $msg ) {
    echo '<div id="woocommerce_errors" class="error fade"><p>ERRORE: '.$msg.'</p></div>';
  }

  /**
   * Create a SOAP client using the specified URL
   */
  function get_soap_client( $url ) {
    try {
      $client = new SoapClient( $url );
    }
    catch ( Exception $e ) {
      $err = sprintf( __( 'Soap Client Request Exception with error %s' ), $e->getMessage() );
      $this->wc_add_error( $err );
      $this->log_add( '[FATAL ERROR]: ' . $err );

      return false;
    }

    return $client;
  }


  /**
   * La maggior parte dei gateway che creo utilizzano la libreria SOAP di PHP.
   * Se questa non è installata nel sistema il gateway non si può utilizzare.
   */
  function check_fatal_soap( $plugin_name ) {
    if ( ! extension_loaded( 'soap' ) ) {
      $this->show_error( 'Per poter utilizzare <strong>'.$plugin_name.'</strong> la libreria SOAP client di PHP deve essere abilitata!' );
      return false;
    }
    return true;
  }

  /**
   * Le query string restituite da alcuni gateway sono molto lunghe e se si utilizza
   * suhosin con un valore basso nella get, allora verranno troncate.
   * Per questo motivo è meglio bloccare l'utilizzo del plugin.
   * Thanks to Andrea Cardinali
   */
  function check_fatal_suhosin( $plugin_name ) {
    if ( is_numeric( @ini_get( 'suhosin.get.max_value_length' ) ) && ( @ini_get( 'suhosin.get.max_value_length' ) < 1024 ) ) {
      $err_suhosin = 'Sul tuo server è presente <a href="http://www.hardened-php.net/suhosin/index.html" target="_blank">PHP Suhosin</a>.<br>Devi aumentare il valore di';
      $err_suhosin.= ' <a href="http://suhosin.org/stories/configuration.html#suhosin-get-max-value-length" target="_blank">suhosin.get.max_value_length</a> almeno a 1024';
      $err_suhosin.= ', perché <strong>'.$plugin_name.'</strong> utilizza delle query string molto lunghe.<br>';
      $err_suhosin.= '<strong>'.$plugin_name.'</strong> non potrà essere utilizzato finché non si aumenta tale valore!';

      $this->show_error( $err_suhosin );
      return false;
    }
    return true;
  }

  /**
   * Safely get and trim data from $_POST
   */
  function get_post( $key ) {
    return isset( $_POST[ $key ] ) ? trim( $_POST[ $key ] ) : '';
  }

  /**
   * Send an email to the admin
   */
  function admin_mail( $subject, $message ) {
    if ( ! $this->send_debug_email ) return;
    global $woocommerce;
    $mailer = $woocommerce->mailer();
    $mailer->wrap_message( $subject, $message );
    $mailer->send( get_settings( 'admin_email' ), '['.$this->plugin_name.'] ' . $subject, $message );
  }

  /**
   * Check for plugin updates
   */
  function mmascia_start_uvs( $with_debug = false ) {
    // Esegui solo lato admin e se l'utente può aggiornare i plugin
    if ( ! is_admin() || ! current_user_can( 'update_plugins' ) ) return;

    // Verifica che non venga eseguito in tutte le pagine
    // (is_admin è valido anche quando vengono effettuate le chiamate ad admin-ajax)
    global $pagenow;
    if ( in_array( $pagenow, array( 'plugins.php', 'update-core.php' ) )
      || ( $pagenow == 'admin-ajax.php' && isset( $_GET['action'] ) && $_GET['action'] == $this->plugin_slug.'_license' ) ) {

      if ( ! class_exists( 'MMascia_Update_Validation_System' ) ) require_once 'uvs.php';
      if ( class_exists( 'MMascia_Update_Validation_System' ) ) {

        if ( $with_debug ) {
          error_log( "[MMASCIA_LIB] Checking " . $this->plugin_slug . ' updates / Debug: ' . ($with_debug?'true':'false') );
        }

        $uvs = new MMascia_Update_Validation_System(
          array(
            'api_url'       => 'http://www.mauromascia.com',
            'updater_url'   => 'http://updater.mauromascia.com/api',
            'slug'          => $this->plugin_slug,
            'plugin_path'   => plugin_basename( $this->plugin_main_file ),
            'plugin_url'    => $this->plugin_url
          ),
          $with_debug
        );
      }

    }
  }

}

endif; // ! class_exists( 'WC_Gateway_MM_Lib' )