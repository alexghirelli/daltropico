<?php
/******************************************************************************
 * Plugin Name: WooCommerce @POS (SIA)
 * Plugin URI: http://www.mauromascia.com/shop/product/woocommerce-pos-payment-gateway-sia/
 * Description: Redirect Gateway @POS (SIA) per WooCommerce.
 * Version: 20190710
 * Author: Mauro Mascia
 * Author URI: http://www.mauromascia.com
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Support: info@mauromascia.com
 * Copyright: © 2014-2019 MAURO MASCIA
 */

include_once( 'mmlib/class-wc_gateway_mm_lib.php' );

add_action( 'plugins_loaded', 'init_wc_gateway_atpos_sia' );
function init_wc_gateway_atpos_sia() {
  $mmlib = new WC_Gateway_MM_Lib( __FILE__ );

  if ( ! class_exists( 'WC_Payment_Gateways' ) ) return;
  if ( ! $mmlib->check_fatal_soap( 'WooCommerce @POS/SIA' ) ) return;
  if ( ! $mmlib->check_fatal_suhosin( 'WooCommerce @POS/SIA' ) ) return;

  if ( ! version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
    return array( 'error' => '@POS/SIA richiede WooCommerce versione >= 3.x' );
  }

  $mmlib->mmascia_start_uvs();

  /**
   * Add the gateway to WooCommerce.
   */
  add_filter( 'woocommerce_payment_gateways', 'add_wc_gateway_atpos_sia' );
  function add_wc_gateway_atpos_sia( $methods ) {
    $methods[] = 'WC_Gateway_AtPOS_SIA';
    return $methods;
  }

  class WC_Gateway_AtPOS_SIA extends WC_Payment_Gateway {

    public function __construct() {
      // Load my lib
      $mmlib = new WC_Gateway_MM_Lib( __FILE__ );

      $this->mmlib                     = $mmlib;
      $this->plugin_url                = $mmlib->plugin_url;
      $this->plugin_path               = $mmlib->plugin_path;
      $this->id                        = $mmlib->plugin_slug;
      $this->plugin_slug_dashed        = $mmlib->plugin_slug_dashed;
      $this->textdomain                = $this->id;
      $this->logfile                   = $this->id;
      $this->logo                      = $this->plugin_url.'images/gw-logo.png';
      $this->icon                      = $this->plugin_url.'images/gw-cards.png';
      $this->method_title              = '@POS (SIA)';

      $this->error_messages            = array();

      $this->mmlib->__init_gateway( $this );

      // Don't output a payment_box containing direct payment form
      $this->has_fields = false;

      // Define user set variables
      $this->title                 = __( $this->settings['title'] );
      $this->description           = __( $this->settings['description'] );
      $this->idnegozio             = $this->settings['idnegozio'];
      $this->email_esercente       = $this->settings['email_esercente'];
      $this->stringa_segreta_avvio = $this->settings['stringa_segreta_avvio'];
      $this->stringa_segreta_esito = $this->settings['stringa_segreta_esito'];
      $this->process_url           = $this->settings['process_url'] == "yes" ? true : false;

      // get a cleaned prefix
      $this->order_prefix = $this->get_cleaned_prefix();

      $this->debug = $this->settings['debug'] == "yes" ? true : false;

      // Set Web Service process url to test or real
      if ( $this->process_url ) {
        $this->liveurl = "https://atpostest.ssb.it/atpos/pagamenti/main";
      }
      else {
        $this->liveurl = "https://atpos.ssb.it/atpos/pagamenti/main";
      }

      $this->mmlib->set_gw( $this );

      // Actions.
      add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page' ) );
      add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
    }

    // validate order's prefix
    function get_cleaned_prefix() {
      if ( empty( $this->settings['order_prefix'] ) ) return '';

      // allows only alphanumeric charactes
      $prefix = preg_replace( "/[^A-Za-z0-9]/", '', $this->settings['order_prefix'] );

      // max 15 char
      $prefix = substr( $prefix, 0, 15 );

      // Update the order prefix value
      $this->settings['order_prefix'] = $prefix;

      return $this->settings['order_prefix'];
    }

    /**
     * Initialise the language ID between the available languages ITA | EN
     */
    function get_language() {
      return $this->mmlib->get_current_language_2dgtlwr() == 'it' ? 'ITA' : 'EN';
    }


    function atpossia_get_order_total_amount( $order ) {
      $order_total = $order->get_total();

      // Remove the decimals: 23,50 must become 2350
      $total = number_format( $order_total, 2, ".", "" ) * 100;
      if ($total < 10) $total = 10; // minimo 2 cifre!
      return $total;
    }

    /**
     * Admin Panel Options
     */
    public function admin_options() {
      ?>
        <div class="mmascia-gateway-admin-main">
          <div class="mmascia-gateway-message">
            <a href="http://www.mauromascia.com" alt="by Mauro Mascia" target="_blank">
              <img src="<?php echo $this->mmlib->mmlogo; ?>" title="by Mauro Mascia" id="mmascia-logo"/>
            </a>
            <img src="<?php echo $this->logo; ?>" id="mmascia-gateway-logo"/>
            <br>
          <p>
Accetta i pagamenti dalla carte di credito/debito attraverso il @POS/SIA. Dopo che il cliente inserisce le informazioni della sua carta di credito, verrà reindirizzato alla pagina ospitata sui server sicuri di @POS per completare la transazione.
Le informazioni delle carte di credito non verranno memorizzate da nessuna parte in questo sistema, ma verranno inserite nei server sicuri di @POS.
          </p>
          </div>
          <br>
          <div class="mmascia-gateway-message mmascia-gateway-form">
              <table class="form-table"><?php $this->generate_settings_html(); ?></table>
          </div>
        </div>
      <?php
    }

    /**
     * Generate the receipt page.
     */
    function receipt_page( $order_id ) {
      $order = wc_get_order( $order_id );
      $input_params = $this->get_params( $order );
      echo $this->mmlib->get_gw_form( $this->liveurl, "POST", $input_params, $order );
    }

    /**
     * Define parameters.
     *
     * @param object $order
     */
    function get_params( $order ) {
      $p_order_id = $this->set_order_id( $order->get_id() );

      // Define parameters
      $p = array(
        'PAGE' => 'MASTER',

        /*
          Importo espresso nell'unita' minima della valuta (centesimi di euro).
          Lunghezza minima 2 massima 8
        */
        'IMPORTO' => $this->atpossia_get_order_total_amount( $order ),

        /*
          Valuta: codice ISO (EUR = 978)
        */
        'VALUTA' => $this->get_currency( $order ),

        /*
          Identificativo univoco dell'ordine: deve essere un codice alfanumerico lungo al massimo 50 caratteri.
          La sua univocita' deve essere garantita per almeno 5 anni.
          I caratteri ammessi sono lettere, cifre, - e _
          Viene applicata la regular expression [a-zA-Z0-9\-_]
        */
        'NUMORD' => $p_order_id,

        /*
          Identificatore del negozio del merchant assegnato dalla BANCA, Codice Riconoscimento Negozio(CRN)
        */
        'IDNEGOZIO' => $this->idnegozio,

        /*
          URL completa verso la quale eseguire una redirect per rimandare l'utente al negozio
          (puo' comprendere tutti gli eventuali parametri da passare) nel caso di annullamento del
          processo di pagamento.
          Lunghezza massima 254 caratteri
        */
        'URLBACK' => add_query_arg( array(
            'ESITO' => 'ANNULLA',
            'NUMORD' => $p_order_id
          ), $this->mmlib->wc_url( $order, 'order_failed' ) ),

        /*
          URL completa verso la quale redirigere il browser del cliente a transazione avvenuta con successo
          (puo' comprendere tutti gli eventuali parametri da passare).
          Il sistema appende ad essa i parametri dell'esito.
          Lunghezza massima 254 caratteri
        */
        'URLDONE' => $this->mmlib->wc_url( $order, 'order_received' ),

        /*
          URL del merchant system verso la quale SSB effettua la GET o POST di conferma dell'avvenuto pagamento
          (puo' contenere eventuali parametri impostati dal negozio). Il sistema appende ad essa i parametri dell'esito.
          Lunghezza massima 400 caratteri
        */
        'URLMS' => add_query_arg( 'wc-api', 'WC_Gateway_AtPOS_SIA', home_url( '/' ) ),

        /*
          Tipo di contabilizzazione da utilizzare per questo ordine:
          - D differita
          - I immediata
        */
        'TCONTAB' => 'I',

        /*
          Tipo di autorizzazione da utilizzare per questo ordine:
          - D differita
          - I immediata
        */
        'TAUTOR' => 'I',

        /*
          Lingua nella quale devono essere mostrati i messaggi di interazione con l'utente finale.
          Il campo e' facoltativo; di default la lingua e' quella Italiana.
          Attualmente sono disponibili:
          - ITA italiano
          - EN inglese
        */
        'LINGUA' => $this->get_language(),

        /*
          Contiene l'indirizzo e-mail al quale inviare la e-mail di esito della transazione.
          Se non e' presente viene utilizzato quello disponibile nella anagrafica SSB del negozio.
          Lunghezza minima 7 caratteri alfanumerici massima 50
        */
        'EMAILESERC' => $this->email_esercente,

        // Indirizzo di e-mail del cliente.
        // Lunghezza minima 7 caratteri alfanumerici massima 50.
        'EMAIL' => $order->get_billing_email(),

        // Al termine del pagamento effettua un redirect automatico al sito del commerciante.
        // Questo previene il problema dell'acquirente che non fa click su "Completa l'ordine".
        'OPTIONS' => 'G'
      );

      /*
        MAC
        Campo di firma della transazione: rende immodificabile da parte dell'utente finale i dati dell'ordine.
        L'ordine con il quale appaiono i campi e', ovviamente, fondamentale.
        Un esempio di tale stringa potrebbe essere:

        URLMS=http://www.dominio.it/ok.asp?par=45&nord=23684&URLDONE=http://www.dominio.it/negozio.asp?par=4
        5&nord=23684&NUMORD=A4845b2&IDNEGOZIO=123456789012345&IMPORTO=100&VALUTA=978&TCONTAB=I&TAUTOR=D&Absd
        830923fk32.....
      */
      $mac = array(
        'URLMS=' .     $p['URLMS'],
        'URLDONE=' .   $p['URLDONE'],
        'NUMORD=' .    $p['NUMORD'],
        'IDNEGOZIO=' . $p['IDNEGOZIO'],
        'IMPORTO=' .   $p['IMPORTO'],
        'VALUTA=' .    $p['VALUTA'],
        'TCONTAB=' .   $p['TCONTAB'],
        'TAUTOR=' .    $p['TAUTOR'],
        'OPTIONS=' .   $p['OPTIONS'],
        $this->stringa_segreta_avvio
      );

      $p['MAC'] = strtoupper( sha1( implode( '&', $mac ) ) );

      $this->mmlib->log_add( "[INFO]: Parameters: " . var_export( $p, true ) );

      return $p;
    }

    // It is possible to manage more currencies?
    function get_currency( $order ) {
      // $this->mmlib->get_order_currency( $order );
      return '978';
    }

    /**
     * Process the payment and return the result
     */
    function process_payment( $order_id ) {
      $this->mmlib->log_add( "[process_payment]: Processing payment for order n. " . $order_id );

      $order = wc_get_order( $order_id );
      $params = $this->get_params( $order );
      $query_params = http_build_query( $params, '', '&' );

      return array(
        'result' => 'success',
        'redirect' => $this->liveurl . '?' . $query_params,
      );
    }

    /**
     * Returns the right ID used by WooCommerce, removing the prefix used to
     * prevent duplicate order ids
     */
    function get_order_id( $raw_order_id ) {
      if ( substr( $raw_order_id, 0, strlen( $this->order_prefix ) ) == $this->order_prefix ) {
        // Remove the prefix from the beginning of the prefixed order id.
        return substr( $raw_order_id, strlen( $this->order_prefix ) );
      }
      else {
        $this->mmlib->log_add( "[get_order_id]: ERROR on get_order_id" );
        $this->mmlib->log_add( "[get_order_id]: raw_order_id: " . $raw_order_id );
        $this->mmlib->log_add( "[get_order_id]: order_prefix: " . $this->order_prefix );

        return 0;
      }
    }

    /**
     * Sets a prefixed ID, useful if the same account is used in multiple
     * stores. This prevent to process the same order ID and prevents the
     * error: "duplicate order".
     */
    function set_order_id( $order_id ) {
      return $this->order_prefix . $order_id;
    }

    /**
     * Check for valid response
     */
    function check_gateway_response() {
      if ( !empty( $_GET['ESITO'] ) && !empty( $_GET['NUMORD'] ) && !empty( $_GET['AUT'] ) ) {
        $order = wc_get_order( $this->get_order_id( $_GET['NUMORD'] ) );
        $order_id = $this->set_order_id( $order->get_id() );
        $order_status = $order->get_status();

        // Transazione annullata
        if ( $_GET['ESITO'] == 'ANNULLA' ) {
          $this->msg['class'] = 'woocommerce_error';
          $this->msg['message'] = sprintf( __( 'The order %s has been canceled.', 'woocommerce_atpos_sia' ), $order_id );

          // Quando si clicca su "torna all'esercente" potremo anche cancellare l'ordine...
          // $order->update_status( 'cancelled' );

          $order->add_order_note( $this->msg['message'] );
          $this->mmlib->log_add( "[check_atpos_response ERROR]: " . $this->msg['message'] );
          add_action( 'the_content', array( &$this, 'show_message' ) );

          return false;
        }

        // la transazione richiesta è già stata completata
        if ( $order_status == 'completed' || $order_status == 'processing' ) {
          $this->redirect( $order, true );
        }

        if ( $order_status !== 'cancelled' ) {
          if ( $_GET['ESITO'] == '00' ) {

            // Verify if the response code is the same
            // Some values are took from the GET because they comes from @POS
            // @see pages 93-94 and 118

            $our_mac_to_hash = array(
              'NUMORD=' . $order_id,
              'IDNEGOZIO=' . $this->idnegozio,
              'AUT=' . $_GET['AUT'],
              'IMPORTO=' . $this->atpossia_get_order_total_amount( $order ),
              'VALUTA=' . $this->get_currency( $order ),
              'IDTRANS=' . $_GET['IDTRANS'],
              'TCONTAB=' . 'I',
              'TAUTOR=' . 'I',
              'ESITO=' . $_GET['ESITO'],
              'BPW_TIPO_TRANSAZIONE=' . $_GET['BPW_TIPO_TRANSAZIONE'],
              $this->stringa_segreta_esito
            );

            $this->mmlib->log_add( "[check_atpos_response RESPONSES]: " . implode( ' - ', $our_mac_to_hash ) );

            $our_mac_hashed = strtoupper( sha1( implode( '&', $our_mac_to_hash ) ) );

            // Check the authenticity of the received parameters.
            if ( $our_mac_hashed == $_GET['MAC'] ) {
              if ( $order_status !== 'completed' ) {
                if ( $order_status == 'processing' ) {
                  // This is the second call - do nothing
                }
                else {
                  $order_link = '<a href="' . $this->mmlib->wc_url( $order, 'view_order' ) . '">' . $order_id . '</a>';
                  $this->msg['message'] = sprintf( __( 'Thank you for shopping with us. Your transaction %s has been processed correctly. We will ship your order to you soon.', 'woocommerce_atpos_sia' ), $order_link );
                  $this->msg['class'] = 'woocommerce_message';
                  $mes = sprintf( __( 'The order %s was placed successfully.', 'woocommerce_atpos_sia' ), $order_id );

                  // Update order status, add admin order note and empty the cart
                  $this->mmlib->wc_order_completed( $order, $mes, $_GET['AUT'] );

                  $this->redirect( $order, true );
                }
              }
            }
            else {
              $this->msg['class'] = 'woocommerce_error';
              $this->msg['message'] =  __( 'MAC differs:', 'woocommerce_atpos_sia' ) . ' ' . $order_id . ' Error: ' . $_GET['ESITO'];
              $this->mmlib->log_add( "[check_atpos_response FATAL ERROR]: " . $this->msg['message'] );
            }
          }
          else {
            // Update order status and add admin order note
            $this->msg['message'] = sprintf( __( 'The order %s has failed with status: %s', 'woocommerce_atpos_sia' ), $order_id, $_GET['ESITO'] );
            $this->msg['class'] = 'woocommerce_error';

            $order->update_status( 'failed', $this->msg['message'] );

            $this->mmlib->log_add( "[check_atpos_response ERROR]: " . $this->msg['message'] );

            $this->redirect( $order, false );
          }
        }
        else {
          $this->msg['class'] = 'woocommerce_error';
          $this->msg['message'] = sprintf( __( 'The order %s has been canceled.', 'woocommerce_atpos_sia' ), $order_id );
        }

        add_action( 'the_content', array( &$this, 'show_message' ) );
      }
    }

    function redirect( $order, $ok = true ) {

      $url = $ok
        ? $this->mmlib->wc_url( $order, 'order_received' )
        : $this->mmlib->wc_url( $order, 'order_failed' );

      wp_redirect( $url );
      die();
    }

    function show_message( $content = "" ) {
      if ( isset( $this->msg ) ) {
        $message = <<<HTML
          <div class="atpos_sia-box {$this->msg['class']}">
            {$this->msg['message']}
          </div>
HTML;
      }
      else {
        $message = '';
      }

      return $message . $content;
    }

  }

}

/*
 * Registers any arbitrary API request handler.
 * We need to check the arguments when the user is redirected on the original web site.
 * This differs from the WooCommerce 2.0-way in which some services (like IPN)
 * can be managed using the wc-api parameter.
 *
 * Warning: wc_gateway_atpos_sia_check_gateway_response_new_wc must be executed after the
 *
 * add_action( 'init', array( 'WC_Emails', 'init_transactional_emails' ) );
 *
 * added in WooCommerce 2.3.x and for that reason it needs an higher priority.
 *
 * @see:
 * - http://docs.woothemes.com/document/payment-gateway-api/
 * - http://www.skyverge.com/blog/migrating-your-plugin-woocommerce-2-0/#payment_gateways
 * - http://wcdocs.woothemes.com/version-notes/woocommerce-1-6-6-2-0-plugin-and-theme-compatibility/
 * - http://www.mrova.com/lets-create-a-payment-gateway-plugin-payu-for-woocommerce/
 */
add_action( 'init', 'wc_gateway_atpos_sia_check_gateway_response_new_wc', 999 );
function wc_gateway_atpos_sia_check_gateway_response_new_wc() {
  if ( !empty( $_GET['ESITO'] ) && !empty( $_GET['NUMORD'] ) && !empty( $_GET['AUT'] ) ) {
    $atpos_sia = new WC_Gateway_AtPOS_SIA();
    $atpos_sia->check_gateway_response();
  }
}
