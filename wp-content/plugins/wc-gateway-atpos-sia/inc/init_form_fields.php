<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$base_stuff = array(
	'enabled' => array(
		'title' => "Abilita/Disabilita:",
		'type' => 'checkbox',
		'label' => 'Abilita @POS di SIA',
		'default' => 'yes'
	),
	'title' => array(
		'title' => "Titolo:",
		'type' => 'text',
		'description' => "Il titolo che l'utente visualizza durante il checkout.",
		'default' => "@POS (SIA)"
	),
	'description' => array(
		'title' => "Descrizione:",
		'type' => 'textarea',
		'description' => "La descrizione che l'utente visualizza durante il checkout.",
		'default' => "Paga in tutta sicurezza con @POS."
	),
);

$gateway = array(
    'idnegozio' => array(
      'title' => 'ID Negozio',
      'type' => 'text',
      'description' => 'Identificatore del negozio del merchant assegnato dalla BANCA, Codice Riconoscimento Negozio (CRN).',
      'default' => ''
    ),
    'email_esercente' => array(
      'title' => 'Email Esercente',
      'type' => 'text',
      'description' => '',
      'default' => ''
    ),
    'stringa_segreta_avvio' => array(
      'title' => 'Stringa Segreta Avvio:',
      'type' => 'text',
      'description' => '',
      'default' => ''
    ),
    'stringa_segreta_esito' => array(
      'title' => 'Stringa Segreta Esito:',
      'type' => 'text',
      'description' => '',
      'default' => ''
    ),
);

$options = array(
	'order_prefix' => array(
		'title' => "Prefisso ordine:",
		'type' => 'text',
		'description' => "Specificare il prefisso utilizzato per gli ordini: questo impedisce di elaborare lo stesso ID ordine in più negozi quando si utilizza lo stesso account @POS. È possibile inserire fino a 15 caratteri alfanumerici: tutti gli altri simboli/caratteri verranno automaticamente rimossi.",
		'default' => ''
	),
);

$cards = $this->get_cards_settings();

$testing = array(
	'testing' => array(
		'title' => "Test del Gateway",
		'type' => 'title',
		'description' => '',
	),
    'process_url' => array(
      'title' => 'Sandbox/test mode:',
      'type' => 'checkbox',
      'label' => 'Abilita ambiente di test',
      'default' => 'yes'
    ),
	'debug' => array(
		'title' => 'Debug Log:',
		'type' => 'checkbox',
		'label' => "Registra gli eventi del gateway in:<br>" . $this->plugin_logfile_name,
		'default' => 'no',
	),
);

return array_merge( $base_stuff, $gateway, $options, $cards, $testing );
