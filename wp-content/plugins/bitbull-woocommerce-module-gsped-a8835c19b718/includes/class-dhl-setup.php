<?php

/**
 * Fired during plugin activation
 *
 * @link       https://gsped.com/
 * @since      1.0.0
 *
 * @package    Gsped
 * @subpackage Gsped/includes
 */

/**
 * Fired during plugin activation/deactivation.
 *
 * This class defines all code necessary to run during the plugin's activation/deactivation.
 *
 * @since      1.0.0
 * @package    DHL
 * @subpackage DHL/includes
 */
class Gsped_Setup {

    const USER_MAME = 'dhl-api';
    const USER_EMAIL = 'info@dhl.com';

	/**
	 * Activate plugin.
	 *
	 * @since    1.0.0
	 */
	public function activate() {

        // Check if WooCommerce is active
        if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' )))) {
            echo '<strong>'.__('WooCommerce not installed', 'gsped').'</strong>';
            exit;
        }

        // Create API user
        $userId = username_exists( self::USER_MAME );
        if ( $userId === false && email_exists(self::USER_EMAIL) == false ) {
            $random_password = wp_generate_password( $length=30, $include_standard_special_chars=true );
            $userId = wp_create_user( self::USER_MAME, $random_password, self::USER_EMAIL );
            $user = new WP_User($userId);
            $user->set_role('shop_manager');

            Gsped_Settings::setSettingsData(array(
                'soap_user' => self::USER_MAME,
                'soap_key' => $random_password,
            ));
        }

	}

    /**
     * Deactivate plugin.
     *
     * @since    1.0.0
     */
    public function deactivate() {

        delete_option( Gsped_Settings::SETTINGS_NAME);

        $userId = username_exists( self::USER_MAME);
        if ($userId !== false) {
            wp_delete_user($userId);
        }

    }

}
