<?php

/**
 * Settings Storage
 *
 * @link       https://gsped.com/
 * @since      1.0.0
 *
 * @package    Gsped
 * @subpackage Gsped/includes
 */

/**
 * Settings Storage
 *
 * This class store and retrieve settings from the database.
 *
 * @since      1.0.0
 * @package    DHL
 * @subpackage DHL/includes
 */
class Gsped_Settings {

    /**
     * Gsped settings name
     *
     * @since    1.0.0
     */
    const SETTINGS_NAME = 'wc_settings_tab_gsped_data';

    /**
     * Get settings data
     *
     * @since    1.0.0
     * @return array
     */
    public static function getSettingsData() {

        $data = get_option(self::SETTINGS_NAME);

        $currentSettings = (array) json_decode($data);
        $default = array(
            'soap_user' => '',
            'soap_key' => '',
            'token' => null,
            'insuranceEnabled' => 0
        );
        return array_merge($default, $currentSettings == null ? array():$currentSettings);

    }

    /**
     * Set settings data
     *
     * @since    1.0.0
     * @param $settings array
     */
    public static function setSettingsData($settings) {

        $oldData = self::getSettingsData();
        $data = json_encode(array_merge($oldData, $settings));

        update_option(self::SETTINGS_NAME, $data);

    }

}
