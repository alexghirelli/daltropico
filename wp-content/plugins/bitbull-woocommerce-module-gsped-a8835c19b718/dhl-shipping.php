<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Dhl
 *
 * @wordpress-plugin
 * Plugin Name:       DHL
 * Description:       DHL shipping plugin for WooCommerce.
 * Version:           dev
 * Author:            DHL
 * Author URI:        http://www.dhl.it/it/express.html
 * Text Domain:       gsped
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dhl-setup.php
 *
 */
function activate_gsped() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dhl-setup.php';
	$setup = new Gsped_Setup();
    $setup->activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dhl-setup.php
 *
 */
function deactivate_gsped() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dhl-setup.php';
    $setup = new Gsped_Setup();
    $setup->deactivate();
}

register_activation_hook( __FILE__, 'activate_gsped' );
register_deactivation_hook( __FILE__, 'deactivate_gsped' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 *
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dhl.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gsped() {

	$plugin = new Gsped();
	$plugin->run();

}
run_gsped();
