DHL Extension for WooCommerce
===========================

Extension to handle the default configurations for DHL shipments and returns managed through the [Gsped](https://gsped.com/) platform

## Requirements

This plugin require [WooCommerce](https://it.wordpress.org/plugins/woocommerce/) plugin to be installed and activated.

## Installation Instructions

You can install the extension with following methods:

### Manual installation

Extract the plugin archive and copy the directory `dhl-shipping` into `/wp-content/plugins/`.

### Install using WordPress Admin Plugin Upload

You need to go to WordPress admin area and visit Plugins » Add New page. After that, click on the Upload Plugin button on top of the page.

![Plugins » Add New page](./docs/wp-plugin-addnew.png)

This will bring you to the plugin upload page. Here you need to click on the choose file button and select the plugin file you downloaded earlier to your computer.

After you have selected the file, you need to click on the install now button.

![Plugins » Add New page](./docs/wp-plugin-upload.png)

WordPress will now upload the plugin file from your computer and install it for you. You will see a success message like this after installation is finished.

## Module Configuration

Once installed, visit Plugins page and you need to click on the Activate Plugin link to start using the plugin.

During the installation of the module, an user is created and the credentials are automatically saved in configuration.

Under WooCommerce -> Settings -> Shipping -> Shipping options, the user can enter and modify the configurations clicking on _Update configurations_

![WooCommerce shipping configuration](./docs/wp-plugin-config.png)

Each configuration can be modified for the single shipment from the order page

![Dhl configuration](./docs/wp-plugin-settings-modal.png)

### Options

__1__ __Default shipping settings__

- Insert the DHL user code in the required __User Code__ field required
- __Number of packages__ to ship
- __Weight__ and __volume__
- Default Label format required
- Default __domestic__ and __international__ service
- __Insurance__ set yes to enable
- Short __package description__

__2__ __Order Status__

- The user can select between all the order states of the current Magento instance those in which it is possible to start a shipment.
The shipment button will be present on the order page only if the order is in one of the selected states.

__3__ __Cash on delivery options__

- Select all payment methods that provides Cash on delivery
- Select the default cashout mode for the Cash on delivery

__4__ Parameters for __automatic pick up__

- Set Yes to enable __automatic pick up__
- Maximum time for pick-up requirement in a day (from 10 to 14)
- Ready for goods Time (from 14 to 18)

__5__ __Default sender__ data

### Creation of the shipment

The user can create a shipment from the order page.
If the order status is among those enabled in the configuration the button 'Ship with DHL' will appear in the order page.

Clicking on the button will open a modal to confirm or change the shipping configurations. The content of the modal is provided by the Gsped platform.

![Ship button](./docs/wp-plugin-order-page-ship.png)

### Track

The button to track the shipment will appear on the order page only after a shipment has already been created.

![Ship button](./docs/wp-plugin-order-page-track.png)

### Return

The button to start the return will appear on the order page only after a shipment has already been created.

![Return button](./docs/wp-plugin-order-page-return.png)

### List shippings

A new menu item named __DHL Shipping__ is showed under WooCommerce menu, click on it to show the list and menage shippings.

![Shippings list](./docs/wp-plugin-order-page-list.png)
