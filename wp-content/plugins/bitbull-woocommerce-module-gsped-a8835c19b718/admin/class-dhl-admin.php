<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://gsped.com/
 * @since      1.0.0
 *
 * @package    Gsped
 * @subpackage Gsped/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    DHL
 * @subpackage DHL/admin
 */
class Gsped_Admin {

    /**
     * Order post type
     *
     * @since    1.0.0
     */
    const ORDER_POST_TYPE = 'shop_order';

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

    /**
     * Get shipping settings
     *
     * @since    1.0.0
     */
	private function get_shipping_settings() {
	    return array(
            'gsped_setting' => array(
                'name' => __( 'DHL express', 'gsped' ),
                'type' => 'gsped_textarea',
                'desc' => '',
                'id'   => Gsped_Settings::SETTINGS_NAME
            )
        );
    }

    /**
     * Get Payment methods
     *
     * @since    1.0.0
     * @return array
     */
    private function getPaymentMethods () {
        $gateways = WC()->payment_gateways->get_available_payment_gateways();
        $data = [];

        foreach( $gateways as $gateway ) {
            if( $gateway->enabled == 'yes' ) {
                $data[$gateway->id] = $gateway->title;
            }
        }
        return $data;
    }

    /**
     * Get Order statuses
     *
     * @since    1.0.0
     * @return array
     */
    private function getOrderStatuses () {
        $statuses = wc_get_order_statuses();
        $data = [];

        foreach( $statuses as $value => $label ) {
            $data[] = array(
                'label' => $label,
                'value' => $value,
            );
        }
        return $data;
    }

    /**
     * Is Order page
     *
     * @since    1.0.0
     * @return boolean
     */
    private function isOrderPage() {
        $currentScreen = get_current_screen();
        return $currentScreen->id === self::ORDER_POST_TYPE;
    }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gsped-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gsped-admin.js', array('jquery'), $this->version, true );

	}

    /**
     * Save changed user password
     *
     * @param $user_id array
     * @return array
     * @since    1.0.0
     */
	public function edit_user_profile_update($user_id) {

        $user = get_user_by('ID', $user_id);
        if($user !== false){
            $currentSettings = Gsped_Settings::getSettingsData();
            if($user->user_login == $currentSettings['soap_user']){
                $currentSettings['soap_key'] = $_POST['pass1'];
                Gsped_Settings::setSettingsData($currentSettings);
            }
        }

    }

    /**
     * Add shipping settings
     *
     * @param $settings array
     * @return array
     * @since    1.0.0
     */
	public function add_shipping_settings($settings) {

         array_splice( $settings, sizeof($settings) - 2, 0, $this->get_shipping_settings() );
         return $settings;

    }

    /**
     * Render settings input
     *
     * @param $value array
     * @since    1.0.0
     */
    public function render_shipping_settings_input($value) {
        $option_value = json_encode(Gsped_Settings::getSettingsData());

        ?>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr( $value['id'] ); ?>"><?php echo esc_html( $value['title'] ); ?></label>
            </th>
            <td class="forminp forminp-<?php echo esc_attr( sanitize_title( $value['type'] ) ); ?>">
                <textarea
                        name="<?php echo esc_attr( $value['id'] ); ?>"
                        id="<?php echo esc_attr( $value['id'] ); ?>"
                        style="display: none"
                        class="<?php echo esc_attr( $value['class'] ); ?>"
                        placeholder="<?php echo esc_attr( $value['placeholder'] ); ?>"
                ><?php echo esc_textarea( $option_value );?></textarea>
            </td>
        </tr>
        <?php
    }

    /**
     * Initialize Gsped JS library
     *
     * @since    1.0.0
     */
    public function init_gsped_js_library () {

        $settings = Gsped_Settings::getSettingsData();

        $settings['apiBaseUrl'] = get_rest_url();
        $settings['paymentMethods'] = $this->getPaymentMethods();
        $settings['platformStatuses'] = $this->getOrderStatuses();

        $cashOnDelivery = [];
        if(isset($settings['payment_methods'])) {
            $cashOnDelivery = $settings['payment_methods'];
        }
        $settings['cashOnDelivery'] = $cashOnDelivery;

        if(isset($settings['autologin'])) {
            $settings['token'] = $settings['autologin'];
        }

        if(isset($settings['order_status'])) {
            $settings['statusToShip'] = $settings['order_status'];
        }

        if(isset($settings['user_code_main'])) {
            $settings['userCode'] = $settings['user_code_main'];
        }

        $data['config'] = $settings;
        $data['order'] = [];

        if($this->isOrderPage()){

            $post = get_post();
            $wcOrder = new WC_Order($post->ID);
            if($wcOrder->post !== null){
                $data['order'] = array(
                    'status' => 'wc-'.$wcOrder->get_status(),
                    'paymentMethod' => $wcOrder->payment_method,
                    'grandTotal' => $wcOrder->get_total(),
                    'subtotal' => floatval($wcOrder->get_total()) - floatval($wcOrder->get_total_shipping()),
                    'incrementId' => $post->ID,
                    'email' => $wcOrder->billing_email,
                    'shippingAddress' => array(
                        'firstname' => $wcOrder->shipping_first_name,
                        'lastname' => $wcOrder->shipping_last_name,
                        'address' => $wcOrder->shipping_address_1.$wcOrder->shipping_address_2,
                        'city' => $wcOrder->shipping_city,
                        'country' => $wcOrder->shipping_country,
                        'phone' => $wcOrder->billing_phone,
                        'postcode' => $wcOrder->shipping_postcode
                    )
                );
            }
        }

        ?>
        <script type="text/javascript">
            window.GspedData = <?=json_encode($data)?>
        </script>
        <?php
    }

}
